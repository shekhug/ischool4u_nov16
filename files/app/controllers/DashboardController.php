<?php
class DashboardController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize(); 
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }       
    }
    public function indexAction(){
    	$response = new \Phalcon\Http\Response();
    	 if(!$this->session->has('admin')){
    		return $response->redirect("admin/login");
    	}


    }

}
?>