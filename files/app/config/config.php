<?php

$config = new Phalcon\Config(array(
    'database' => array(
        'adapter' => 'mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => '2489DF45@6hg',
        'dbname' => 'ischool4u_database'
        ),
    'phalcon' => array(
        'controllersDir' => '/../app/controllers/',
        'modelsDir' => '/../app/models/',
        'libraryDir' => '/../app/library/',
        'functionDir' => '/../app/functions/',
        'viewsDir' => '/../app/views/',
        'baseUri' => '/ischool4u/'
        ),
    ));
