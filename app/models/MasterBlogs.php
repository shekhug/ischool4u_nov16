<?php

class MasterBlogs extends \Phalcon\Mvc\Model
{
    public $pid;
    /**
     * @var integer
     */
    public $title;

    /**
     * @var string
     */
    public $meta_description;

    /**
     * @var string
     */
    public $meta_keyword;

    /**
     * @var integer
     */
    public $is_active;

    
    public $content;

    public function getPid()
    {
        return $this->pid;
    }
    
    
}
