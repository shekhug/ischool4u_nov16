<?php

class IndexController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Discover A Smart Marketplace... ');
        parent::initialize();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $fixpack = MasterPackages::find("course='".$user_session['course']."' AND sup_cat='".$user_session['sup_cat']."'");
            $this->view->setVar("fixpack", $fixpack);
        }
    }

    public function indexAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            return $response->redirect("index/dashboard");
        }
        if ($this->session->get("check_mail")) {
            $this->view->setVar("signup_error", $this->session->get("signup_error"));
            $this->view->setVar("check_email", $this->session->get("check_mail"));
            $this->view->setVar("sup_slug", $this->session->get("sup_slug"));
            $this->session->remove("check_mail");
            $this->session->remove("signup_error");
            $this->session->remove("sup_slug");
        } elseif ($this->session->get("login-error")) {
            $error_msg = $this->session->get("login-error");
            $this->view->setVar("login_error", $error_msg);
            $this->session->remove("login-error");
        }
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
        $curs = MasterCourse::find(array("status=1"));
        $this->view->setVar("curs", $curs);
        $allcourse=array();
        foreach($curs as $val){
         $allcourse[$val->scid][]=array("name"=>$val->name); 
     }
     $pkg = MasterPackages::findFirst("test_type='4' AND course='{$user_session['course']}'");
     $this->view->setVar("pkg", $pkg);
     $setting =SiteSetting::findFirst();
     $this->view->setVar("setting", $setting);
     $this->view->setVar("allcurs", $allcourse);
     $condblog = "status=1";
     $arr1 = array(
        $condblog,
        "order" => "id_blog DESC",
        "limit" => "6");
     $blog = MasterBlog::find($arr1);
     $i=0;
     foreach($blog as $bb){
        $blog1[$i]['title']=$bb->title;
        $blog1[$i]['subtitle']=$bb->subtitle;
        $blog1[$i]['slug']=$bb->slug;
        $blog1[$i]['add_date']=$bb->add_date;
        $i++;
    }
    $this->view->setVar("bloglist", $blog1);
}

function registerAction() {
    echo "register";
    exit;
}

function signupAction() {
    $response = new \Phalcon\Http\Response();
    if ($this->request->isPost()) {
        $sitesetting = SiteSetting::findfirst("id_setting=1"); 
        $postval = $this->request->getPost();
        if (!$postval['course'] && !$postval['sup_cat'] && !$postval['first_name'] && !$postval['last_name'] && !$postval['mail_id'] && ($postval['pass'] != '' || $postval['google_id'] != '' || $postval['facebook_id'] != '')) {
            echo 'not working';
            exit();
        }
        $check_email = MasterUsers::findFirst(array("mail_id='" . $postval['mail_id'] . "'"));
        if ($check_email->uid != '') {
            $sup_slug = MasterSupCat::findFirst(array("scid='" . $postval['sup_cat'] . "'"));
            $this->session->set("check_mail", "Email is already Exits.");
            $this->session->set("signup_error", "Signup Error");
            $this->session->set("sup_slug", $sup_slug->slug);
            return $response->redirect("index");
        }
        $beforpass=$postval['pass'];
        $postval['pass'] = base64_encode($postval['pass']);
        $postval['created'] = date('Y-m-d h:i:s');
        $postval['mail_code'] = rand(1000, 2000);
        $postval['is_active'] = 1;
        $postval['mail_verified'] = 0;
        $opt = base64_encode($postval['mail_code']);
        $con = new MasterUsers();
        if ($con->save($postval)) {
                ///add as friend done by satya
                    $signpoint =$sitesetting->new_student;// (int) $signpoint + (int) 50; //point for invited user
                    if ($postval['create_user'] != '') {
                    $inviterpoint = $sitesetting->invite_by; //point for who invite 
                    $postdata['inviteuser'] = array($con->uid);
                    $user = MasterUsers::findFirst(array("md5(uid)='" . $postval['create_user'] . "'"));
                    $chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
                    if ($chkuser->uid != '') {
                        if ($chkuser->accept_friend != '') {
                            $userall = json_decode($chkuser->accept_friend);
                            $finalarr = array_merge((array) $userall, (array) $postdata['inviteuser']);
                        } else {
                            $finalarr = $postdata['inviteuser'];
                        }
                        $jsondata = json_encode(array_values($finalarr));
                        $sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user->uid}'";
                        $status = $this->modelsManager->executeQuery($sql);
                    } else {
                        $jsondata = json_encode(array_values($postdata['inviteuser']));
                        $sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user->uid}')";
                        $status = $this->modelsManager->executeQuery($sql);
                    }
                    $sql_actpoint = "UPDATE  MasterUserpoint SET points=points+$inviterpoint WHERE uid={$user->uid}";
                    $uo_sign = $this->modelsManager->executeQuery($sql_actpoint);
                    //challenged question
                    if (!empty($postval['quesid'])) {
                        $quesid = base64_decode($postval['quesid']);
                        $add_date = date("y-m-d H:i:s");
                        $cond2 = "(md5(uid)='{$postval['create_user']}' AND questionid='{$quesid}')";
                        $chkuser = MasterChallengeques::findFirst(array($cond2));
                        if (!empty($chkuser->id)) {
                            if (trim($chkuser->challenge_id) != '') {
                                $userdta1 = json_decode($chkuser->challenge_id);
                                $finalarr1 = array_merge((array) $userdta1, (array) $postdata['inviteuser']);
                            } else {
                                $finalarr1 = $postdata['inviteuser'];
                            }
                            $jsondata1 = json_encode(array_values($finalarr1));
                            $sql1 = "UPDATE  MasterChallengeques SET challenge_id = '$jsondata1' WHERE uid='{$user->uid}' AND questionid='{$quesid}'";
                            $status = $this->modelsManager->executeQuery($sql1);
                        } else {
                            $jsondata1 = json_encode(array_values($postdata['inviteuser']));
                            $sql1 = "INSERT INTO  MasterChallengeques (challenge_id,uid,questionid,add_date) VALUES ('$jsondata1','{$user->uid}','{$quesid}','{$add_date}')";
                            $status = $this->modelsManager->executeQuery($sql1);
                        }
                        //replace with new notification with prevoius notification
                       /* $rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 GROUP BY add_date ORDER BY add_date ASC";
                        $cne = $this->modelsManager->executeQuery($rep_sql);
                        $totcnt = $cne->count();
                        $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 ORDER BY add_date ASC LIMIT 0,1";
                        $dele = $this->modelsManager->executeQuery($del_sql);
                        if ($totcnt > 1) {
                            foreach ($dele as $deval) {
                                $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                                $this->modelsManager->executeQuery($deldate);
                            }
                        }*/
                        //add to notification table
                        $message = "challenged a question";
                        $link =$postval['quesid'];// addslashes(htmlentities(BASEURL . "question/challenged_question/" . $postval['quesid']));
                        $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$user->uid}','{$con->uid}','{$message}','{$add_date}','0','2','{$link}',1)";
                        $getnot = $this->modelsManager->executeQuery($sqlnot);
                    }
                    
                }
                $sql_signpoint = "INSERT INTO  MasterUserpoint (points,uid) VALUES ($signpoint,'{$con->uid}')";
                $status_sign = $this->modelsManager->executeQuery($sql_signpoint);
                //remove gmail session after with gmail login
                $this->session->remove('gmailreg');
                /// end here satya code
                //redirct to dashboard after registration
                $this->session->set('user', array(
                    'id' => $con->uid,
                    'type' => 'user',
                    'name' => $con->first_name . ' ' . $con->last_name,
                    'course' => $con->course,
                    'sup_cat' => $con->sup_cat
                    ));

//                  if($postval['pass']!=''){
//                       $this->getDI()->getMail()->send(
//                        array(
//                    $use => $use
//                        ), "Successfully Register", 'success', array(
//                    'confirmUrl' => 'Your login credential Username : '.$postval['mail_id'].' and password : '.$beforpass,
//                        )
//                );
//                  }
                if (!empty($postval['quesid'])) {
                    return $response->redirect("question/challenged_question/" . $postval['quesid']."/".$getnot->id); //redirect to challenged question
                } else {
                    return $response->redirect("index/dashboard");
                }
                
                /* no verification of email  during registration */
                //return $response->redirect("index/verify/" . $opt);
            }
        }
        return $response->redirect("index");
    }
    function checkloginAction(){
       $response = new \Phalcon\Http\Response(); 
         if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['pass'] = base64_encode($postval['pass']);
            $user = MasterUsers::findFirst(array("mail_id='" . $postval['email'] . "' AND pass='" . $postval['pass'] . "' "));
            $mvcheck = MasterUsers::findFirst(array("uid='" . $user->uid . "'"));
            $points = MasterUserpoint::findFirst("uid='".$user->uid."' ");
            if ($mvcheck->uid != '') {
                echo 1;exit;
            }else{
                echo 2;exit;
            }
        }
    }
    function loginAction($userid='') {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['pass'] = base64_encode($postval['pass']);
            $user = MasterUsers::findFirst(array("mail_id='" . $postval['email'] . "' AND pass='" . $postval['pass'] . "' "));
            $mvcheck = MasterUsers::findFirst(array("uid='" . $user->uid . "'"));
            $points = MasterUserpoint::findFirst("uid='".$user->uid."' ");
            if ($mvcheck->uid != '') {
                if ($postval['like']) {
                    $like['id_blog'] = $postval['like'];
                    $like['user_id'] = $mvcheck->uid;
                    $like['status'] = 1;
                    $likeinfo = BlogLike::findFirst(array("user_id='" . $like['user_id'] . "' AND id_blog='" . $like['id_blog'] . "' "));
                    if (empty($likeinfo)) {
                        $phql = "UPDATE MasterBlog SET totallike =totallike+1 where id_blog=" . $postval['like'] . "";
                        $this->modelsManager->executeQuery($phql);
                        $lik = new BlogLike();
                        $lik->save($like);
                    }
                }
                $this->session->set('user', array(
                    'id' => $user->uid,
                    'type' => 'user',
                    'name' => $user->first_name . ' ' . $user->last_name,
                    'course' => $user->course,
                    'sup_cat' => $user->sup_cat,
                    'img' => $user->img,
                    'points' => $points->points
                    ));
                if (!empty($postval['qid'])) {
                    return $response->redirect("index/qodsolution");
                }
                if (!empty($postval['comment'])) {
                    return $response->redirect("index/comment/" . $postval['comment'] . "/");
                }
                if (!empty($postval['like'])) {
                    $blogid = MasterBlog::findFirst(array("id_blog='" . $postval['like'] . "' "));
                    return $response->redirect("index/comment/" . $blogid->slug . "/");
                }
                return $response->redirect("index/dashboard");
            } else {
                $this->session->set("login-error", "Provide correct username and password.");
                return $response->redirect("index");
            }
        }
        if($userid!=''){
            $user = MasterUsers::findFirst(array("uid='" . $userid . "'"));
            $points = MasterUserpoint::findFirst("uid='".$userid."' ");
            if ($user->uid != '') {
                $this->session->set('user', array(
                    'id' => $user->uid,
                    'type' => 'user',
                    'name' => $user->first_name . ' ' . $user->last_name,
                    'course' => $user->course,
                    'sup_cat' => $user->sup_cat,
                    'img' => $user->img,
                    'points' => $points->points
                    ));
            }
            return $response->redirect("index/dashboard");
        }
        return $response->redirect("index");
    }

    function dashboardAction() {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $pack = MasterUserPackage::find("userid='".$user_session['id']."' AND test_type='5'");
        if($pack->count()!=0){
            $package = TRUE;
        }else{
            $package = FALSE;
        }
        if(empty($this->session->get('fullsylabus'))){
            $this->session->set('fullsylabus',$package);
        }
        
        // print_r($user_session);exit();
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name',$course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
        $userdetails = MasterUsers::findFirst(array("uid='" . $user_session['id'] . "'"));
        $this->view->setVar("userdetails", $userdetails);

        //Get user activity
        $cond = "uid='{$user_session['id']}'";
        $chalenge1 = MasterFriendlist::findFirst(array($cond));
        
        $arr = array();
        if(!empty($chalenge1->accept_friend)){
            $arr = json_decode($chalenge1->accept_friend);
        }

        $cond2 = "LOCATE({$user_session['id']},accept_friend)";
        $chalenge2 = MasterFriendlist::find(array($cond2));
        $arr1 = array();
        foreach ($chalenge2 as $val) {
            $arr1[] = $val->uid;
        }
        $mrg = array($user_session['id']); //session id
        $accept_friend = array_merge((array)$arr,(array)$arr1);
        $all_friend = implode(",", array_merge((array) $mrg, (array) $accept_friend));

        $sql_act = "SELECT * FROM MasterNotification WHERE uid IN ({$all_friend}) AND type!=2 GROUP BY id ORDER BY id DESC";
        $res_us = $this->modelsManager->executeQuery($sql_act);
        $i = 0;
        foreach ($res_us as $value) {
            if ($value->notification_type == 1 || $value->notification_type == 11||$value->notification_type==3) {
                $link = BASEURL . "question/challenged_question/" . $value->link . "/" . $value->id;
            } else {
                $link = ($value->link) ? $value->link : "#";
            }
            $data[$i]['add_date'] = date("d-m-y,h:i A", strtotime($value->add_date));
            if ($value->type == 2) {
                if ($value->uid == $user_session['id']) {
                    $userlink = BASEURL . "index/userprofile/" . $value->friend_id;
                    $new = MasterUsers::findFirst(array("uid='" . $value->friend_id . "'"));
                    $data[$i]['img'] = $new->img;
                    $data[$i]['message'] = "You have <a href='" . $link . "'>" . $value->message . "</a> to <a href='#'><b>" . ucfirst($new->first_name) . " " . ucfirst($new->last_name) . "</b></a>";
                } else {
                    if ($value->friend_id == $user_session['id']) {
                        $nextmsg = " with you";
                    } else {
                        $tolink = BASEURL . "index/userprofile/" . $value->friend_id;
                        $tofr = MasterUsers::findFirst(array("uid='" . $value->friend_id . "'"));
                        $nextmsg = " to <a href='#'><b>" . ucfirst($tofr->first_name) . " " . ucfirst($tofr->last_name) . "</b></a>";
                    }
                    $userlink = BASEURL . "index/userprofile/" . $value->uid;
                    $new = MasterUsers::findFirst(array("uid='" . $value->uid . "'"));
                    $data[$i]['img'] = $new->img;
                    $data[$i]['message'] = "<a href='#'><b>" . ucfirst($new->first_name) . " " . ucfirst($new->last_name) . "</b></a> has <a href='" . $link . "'>" . $value->message . "</a>" . $nextmsg;
                }
            } else {
                if ($value->uid == $user_session['id']) {
                    $data[$i]['img'] = $user_session['img'];
                    if($value->notification_type==1){
                        $data[$i]['message'] = "You have <a href='" . $link . "'>" . $value->message . "</a>";
                    }else{
                        $data[$i]['message'] = "You have " . $value->message;
                    }
                } else {
                    $userlink = BASEURL . "index/userprofile/" . $value->uid;
                    $new = MasterUsers::findFirst(array("uid='" . $value->uid . "'"));
                    $data[$i]['img'] = $new->img;
                    
                    $data[$i]['message'] = "<a href='#'><b>" . ucfirst($new->first_name) . " " . ucfirst($new->last_name) . "</b></a> has <a href='" . $link . "'>" . $value->message . "</a>";
                    
                }
            }
            $i++;
        }
        $pkg = MasterPackages::findFirst("test_type='4' AND course='{$user_session['course']}'");
        $this->view->setVar("pkg", $pkg);
        if(empty($this->session->get("fixpack"))){
            $this->session->set('fixpack',$pkg->id);
        }
        $this->view->setVar("data", $data);
        // Follow Action Here
        $follow = $this->followAction();
        $this->view->setVar("follow",$follow);
        $this->view->setVar("follower",$arr1);
        $this->view->setVar("following",$arr);

        // Time spend
        
    }

    function logoutAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $this->session->destroy();
            return $response->redirect("index");
        }
    }

    function verifyAction($id='', $verify='') {
        $response = new \Phalcon\Http\Response();
        if ($id != '') {
            $vcode = base64_decode($id);
            $user = MasterUsers::findFirst(array("mail_code='" . $vcode . "'"));
            $sitesetting = SiteSetting::findfirst("id_setting=1"); 
            if ($user->mail_code != '') {
                $verifypoint =$sitesetting->invite_by; //50 point for mail verification
                $phql = "UPDATE MasterUsers SET mail_code=null,mail_verified=1,is_active=1,modified=now() WHERE uid = '" . $user->uid . "'";
                $this->modelsManager->executeQuery($phql);
                $sql_actpoint = "UPDATE  MasterUserpoint SET points=points+$verifypoint WHERE uid={$user->uid}";
                $uo_sign = $this->modelsManager->executeQuery($sql_actpoint);
                $this->session->set('user', array(
                    'id' => $user->uid,
                    'type' => 'user',
                    'name' => $user->first_name . ' ' . $user->last_name,
                    'course' => $user->course,
                    'sup_cat' => $user->sup_cat,
                    'img' => $user->img
                    ));
                return $response->redirect("index/dashboard");
            }
        } else {
            return $response->redirect("index");
//            echo 'Click the Link to verify your account.';
//            echo "<a href='" . BASEURL . "index/verify/" . $id . "/verify'>" . BASEURL . "index/verify/" . $id . "</a>";
        }
    }

    function profileAction($para='') {
        $request = new \Phalcon\Http\Request();
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $user = $this->session->get("user");
        $course = MasterCourse::findFirst(array("id='" . $user['course'] . "'"));
        $this->view->setVar("course", $course);
        $subject = MasterSubject::find(array("status=1"));
        $this->view->setVar("subject", $subject);
        $user = MasterUsers::findFirst(array("uid='" . $user_session['id'] . "'"));
        $this->view->setVar("user", $user);
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);

        if ($para == 'update') {
            $postval = $this->request->getPost();
            if(isset($_FILES['pic']['name'])){
                    $ext = str_replace(" ","-", $_FILES['pic']['name']);
            }
            $url = BASEDIR.'/public/propic/'.$user_session['id'].$ext;
            if(move_uploaded_file($_FILES['pic']['tmp_name'],$url)){
                $postval['img'] = $user_session['id'].$ext;  
                if($user->img!=''){
                    unlink(BASEDIR.'/public/propic/'.$user->img);
                }              
            }else{
                $postval['img'] = $user->img;
            }
            if(!empty($postval['pass'])){
                $postval['pass'] = base64_encode($postval['pass']);
            }else{
                $postval['pass'] = $user->pass;
            }
            $postval['modified'] = date("Y-m-d h:i:s");
            $postval['uid'] = $user->uid;
            $status = new MasterUsers();
            $status->save($postval);
            $this->session->set('user', array(
                    'id' => $user->uid,
                    'name' => $postval['first_name'] . ' ' . $postval['last_name'],
                    'course' => $postval['course'],
                    'sup_cat' =>$postval['sup_cat'],
                    'img' => $postval['img'],
                    'points' => $user->points
                    ));
            $this->flashSession->success("<div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Profile is Successfully Updated<br> If you change the profile pic it will be changed after login again.</div>");
            return $response->redirect("index/profile");
        }
        
    }

    /* Author:satya
     * create for checking already friend or not
     * daate:23/09/15
     */

    public function checkfriendAction($idcreate='', $invid='', $quesid='') {
        error_reporting(0);
        $this->session->remove("id_create");
        $this->session->remove("tomail");
        $this->session->remove("inviteid");
        $this->session->remove("quesid");
        $cond = "(LOCATE($invid,accept_friend) AND md5(uid)='{$idcreate}') OR (LOCATE('{$idcreate}' ,md5(accept_friend)) AND uid=$invid)";
        $inviteduser = MasterFriendlist::findFirst(array($cond));
        $postdata['inviteuser'] = array($invid);
        $user = MasterUsers::findFirst(array("md5(uid)='" . $idcreate . "'"));

        //challenge question
        if (!empty($quesid)) {
            $quesid = base64_decode($quesid);
            $cond1 = "(LOCATE($invid,challenge_id) AND md5(uid)='{$idcreate}' AND questionid='{$quesid}')";
            $chkuser1 = MasterChallengeques::findFirst(array($cond1));
            $cond2 = "(md5(uid)='{$idcreate}' AND questionid='{$quesid}')";
            $chkuser = MasterChallengeques::findFirst(array($cond2));
            if (empty($chkuser1->id)) {
                if (!empty($chkuser->id)) {
                    if (trim($chkuser->challenge_id) != '') {
                        $userdta = json_decode($chkuser->challenge_id);
                        $finalarr = array_merge((array) $userdta, (array) $postdata['inviteuser']);
                    } else {
                        $finalarr = $postdata['inviteuser'];
                    }
                    $jsondata = json_encode(array_values($finalarr));
                    $sql = "UPDATE  MasterChallengeques SET challenge_id = '$jsondata' WHERE uid='{$user->id}' AND questionid='{$quesid}'";
                    $status = $this->modelsManager->executeQuery($sql);
                } else {
                    $jsondata = json_encode(array_values($postdata['inviteuser']));
                    $sql = "INSERT INTO  MasterChallengeques (challenge_id,uid,questionid,add_date) VALUES ('$jsondata','{$user->id}','{$quesid}','{$add_date}')";
                    $status = $this->modelsManager->executeQuery($sql);
                }
                
                //replace with new notification with prevoius notification
                /*$rep_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 GROUP BY add_date ORDER BY add_date ASC";
                $cne = $this->modelsManager->executeQuery($rep_sql);
                $totcnt = $cne->count();
                $del_sql = "SELECT * FROM MasterNotification WHERE uid='{$user->uid}' AND notification_type=1 ORDER BY add_date ASC LIMIT 0,1";
                $dele = $this->modelsManager->executeQuery($del_sql);
                if ($totcnt > 1) {
                    foreach ($dele as $deval) {
                        $deldate = "DELETE FROM MasterNotification WHERE id='{$deval->id}'";
                        $this->modelsManager->executeQuery($deldate);
                    }
                }*/
                //add notificatio table
                $message = "challenged a question";
                $link =$courses->quesid;// addslashes(htmlentities(BASEURL . "question/challenged_question/" . $courses->quesid));
                $sqlnot = "INSERT INTO  MasterNotification (uid,friend_id,message,add_date,status,type,link,notification_type) VALUES ('{$user->uid}','{$con->uid}','{$message}','{$add_date}','1','2','{$link}',1)";
                $status = $this->modelsManager->executeQuery($sqlnot);
            }
        }
        if ($inviteduser->id == '') {
            $chkuser = MasterFriendlist::findFirst(array("uid='" . $user->uid . "'"));
            if ($chkuser->uid != '') {
                if ($chkuser->accept_friend != '') {
                    $userall = json_decode($chkuser->accept_friend);
                    $finalarr = array_merge((array) $userall, (array) $postdata['inviteuser']);
                } else {
                    $finalarr = $postdata['inviteuser'];
                }
                $jsondata = json_encode(array_values($finalarr));
                $sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user->uid}'";
                $status = $this->modelsManager->executeQuery($sql);
            } else {
                $jsondata = json_encode(array_values($postdata['inviteuser']));
                $sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user->uid}')";
                $status = $this->modelsManager->executeQuery($sql);
            }
            echo 1;
            exit;
        } else {
            echo 0;
            exit;
        }
    }

    function destroysessionAction() {
        $this->session->remove("id_create");
        $this->session->remove("tomail");
        $this->session->remove("inviteid");
        $this->session->remove("quesid");
        echo 1;
        exit;
    }

    function sendingverifymailAction() {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $userdetails = MasterUsers::findFirst(array("uid='" . $user_session['id'] . "'"));
        $opt = base64_encode($userdetails->mail_code);
        $this->getDI()->getMail()->send(
            array(
                $userdetails->mail_id => $userdetails->mail_id
                ), "Verify Your Email to get more point", 'success', array(
                'confirmUrl' => 'index/verify/' . $opt,
                'name'=>$user_session['name']
                )
                );
        $this->flashSession->success("<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Mail has been send Successfully!</div>");
        return $response->redirect("index/dashboard");
    }
    function contactAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['message'] = htmlentities(addslashes($postval['message']));
            $postval['add_date'] = date("Y-m-d H:i:s");
            $Addcont = new ContactUs();
            if ($Addcont->save($postval)) {
                $this->flashSession->success("<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Your message has been sent to us! We will get back to you soon.</div>");
                //send mail here to admin
            } else {
                $this->flashSession->success("<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>There was an error sending your message.</div>");
            }
        }
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }
    function userprofileAction($id=''){
        echo $id;
    }
    function searchdataAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $searchdata = trim($postval['q']);
            $exp=array("a","an","the","in","of","are","is","from","to","be","may","can","all");
            $fil=explode(" ", $searchdata);
            $flitr=array_diff($fil,$exp);
            $dara = implode(",", $flitr);
            $res = MasterQuestionBank::find(array("MATCH(question) AGAINST('{$dara}')"));
            $i = 0;
            $arr ='';
            foreach ($res as $ret) {
                $arr[$ret->questionid]['name'] = $ret->question;
                $arr[$ret->questionid]['link'] = BASEURL . "index/search/" . base64_encode($ret->questionid) . "/1";
                $i++;
            }
            $res1 = MasterMatchQuestion::find(array("MATCH(question) AGAINST('{$dara}')"));
            $j = 0;
            $arr1 ='';
            foreach ($res1 as $ret) {
                $arr1[$ret->questionid]['name'] = $ret->question;
                $arr1[$ret->questionid]['link'] = BASEURL . "index/search/" . base64_encode($ret->questionid) . "/1";
                $j++;
            }
            $res2 = MasterParaQuestion::find(array("MATCH(para) AGAINST('{$dara}')"));
            $k = 0;
            $arr2 ='';
            foreach ($res2 as $ret) {
                $arr2[$ret->questionid]['name'] = $ret->para;
                $arr2[$ret->questionid]['link'] = BASEURL . "index/search/" . base64_encode($ret->questionid) . "/1";
                $k++;
            }
            $res3 = MasterNumericQuestion::find(array("MATCH(question) AGAINST('{$dara}')"));
            $l = 0;
            $arr3 ='';
            foreach ($res3 as $ret) {
                $arr3[$ret->questionid]['name'] = $ret->question;
                $arr3[$ret->questionid]['link'] = BASEURL . "index/search/" . base64_encode($ret->questionid) . "/1";
                $l++;
            }
            $res3 = MasterReasonQuestion::find(array("MATCH(question) AGAINST('{$dara}')"));
            $n = 0;
            $arr4 ='';
            foreach ($res4 as $ret) {
                $arr4[$ret->questionid]['name'] = $ret->question;
                $arr4[$ret->questionid]['link'] = BASEURL . "index/search/" . base64_encode($ret->questionid) . "/1";
                $n++;
            }
            $onlyprac = MasterQuestion::find(array("LOCATE(2,e_type)"));
            $pracarr ='';
            foreach ($onlyprac as $ret) {
                $pracarr[$ret->questionid]['questionid'] = $ret->questionid;
            }
            $allarr=array_merge((array)$arr,(array)$arr1,(array)$arr2,(array)$arr3,(array)$arr4);
            $finalarr=array_diff_key($allarr,$pracarr);
            $this->view->setVar("arr", $finalarr);
            $this->view->setVar("searchdata",$searchdata);
        }
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
    }
    function  searchAction($qid='',$type=''){
        $qid= base64_decode($qid);
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $this->view->setVar("course", $course);
        $qusdetail = MasterQuestion::findFirst(array("questionid='".$qid."'"));
        $subject = MasterSubject::findFirst(array("id='".$qusdetail->subject."'"));
        $subsubject = MasterSubSubject::findFirst(array("ssid='".$qusdetail->subsubject."'"));
        $topic = MasterTopics::findFirst(array("tid='".$qusdetail->topics."'"));
        $this->view->setVar("subject", $subject);
        $this->view->setVar("subsubject", $subsubject);
        $this->view->setVar("topics", $topic);
        $this->view->setVar("questionsIDlist", array($qusdetail->questionid));
    }

    function blogAction($cat='') {
        $response = new \Phalcon\Http\Response();
        $currentPage = $_GET['page'];
        $cond = "status=1";
        if ($cat) {
            $catar = array('Innovation' => "1", 'Exams' => "2", 'Motivation' => "3");
            $cond.=" AND category = " . $catar[$cat];
        }
        if ($_GET['s'] != '') {
            $cond.=" AND title LIKE '" . $_GET['s'] . "%'";
        }
        $arr1 = array(
            $cond,
            "order" => "id_blog DESC");
        $blog = MasterBlog::find($arr1);
        $arr[] = "";
        $i = 0;
        foreach ($blog as $val) {
            $arr[$i]['id_blog'] = $val->id_blog;
            $arr[$i]['title'] = $val->title;
            $arr[$i]['subtitle'] = $val->subtitle;
            $arr[$i]['add_date'] = $val->add_date;
            $arr[$i]['blog'] = $val->blog;
            $arr[$i]['tags'] = $val->tags;
            $arr[$i]['category'] = $val->category;
            $arr[$i]['user_id'] = $val->user_id;
            $arr[$i]['education'] = $val->education;
            $arr[$i]['currently_working'] = $val->currently_working;
            $arr[$i]['profession'] = $val->profession;
            $arr[$i]['profile_image'] = $val->profile_image;
            $arr[$i]['slug'] = $val->slug;
            $arr[$i]['image'] = $val->image;
            $i++;
        }
        $paginator = new \Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $arr,
            "limit" => 6,
            "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        $this->view->setVar("bloglist", $page);
        $latest = array(
            "status = 1",
            "limit" => 6,
            "order" => "id_blog DESC");
        $lat = MasterBlog::find($latest);
        $this->view->setVar("latest", $lat);
        $popular = array(
            "status = 1",
            "limit" => 6,
            "order" => "totallike DESC");
        $pop = MasterBlog::find($popular);
        $this->view->setVar("popular", $pop);
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
        $curs = MasterCourse::find(array("status=1"));
        $this->view->setVar("curs", $curs);
        $allcourse = array();
        foreach ($curs as $val) {
            $allcourse[$val->scid][] = array("name" => $val->name);
        }
        $pkg = MasterPackages::findFirst("test_type='4' AND course='{$user_session['course']}'");
        $this->view->setVar("pkg", $pkg);
        $this->view->setVar("allcurs", $allcourse);
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }

    public function blogdetailsAction($slug='') {
        $response = new \Phalcon\Http\Response();
        if ($slug) {
            $cond.="slug = '" . $slug . "'";
        } else {
            return $response->redirect("index/blog/");
        }
        $arr1 = array("slug = '" . $slug . "'");
        $blog = MasterBlog::findFirst($arr1);
        $this->view->setVar("bloglist", $blog);
        $latest = array(
            "status = 1",
            "limit" => 6,
            "order" => "id_blog DESC");
        $lat = MasterBlog::find($latest);
        $this->view->setVar("latest", $lat);
        $popular = array(
            "status = 1",
            "limit" => 6,
            "order" => "totallike DESC");
        $pop = MasterBlog::find($popular);
        $this->view->setVar("popular", $pop);
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
        $curs = MasterCourse::find(array("status=1"));
        $this->view->setVar("curs", $curs);
        $allcourse = array();
        foreach ($curs as $val) {
            $allcourse[$val->scid][] = array("name" => $val->name);
        }
        $pkg = MasterPackages::findFirst("test_type='4' AND course='{$user_session['course']}'");
        $this->view->setVar("pkg", $pkg);
        $this->view->setVar("allcurs", $allcourse);
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }

    public function commentAction($slug='') {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['user_id'] = $user_session['id'];
            $postval['datetime'] = date("Y-m-d H:i:s");
            $lik = new BlogComment();
            $lik->save($postval);
            return $response->redirect("index/comment/" . $slug . "/");
        }
        $pack = MasterUserPackage::find("userid='" . $user_session['id'] . "' AND test_type='5'");
        if ($pack->count() != 0) {
            $package = TRUE;
        } else {
            $package = FALSE;
        }
        if (empty($this->session->get('fullsylabus'))) {
            $this->session->set('fullsylabus', $package);
        }
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name', $course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
        $userdetails = MasterUsers::findFirst(array("uid='" . $user_session['id'] . "'"));
        $this->view->setVar("userdetails", $userdetails);
        $arr1 = array("slug = '" . $slug . "'");
        $blog = MasterBlog::findFirst($arr1);
        $this->view->setVar("bloglist", $blog);
    }

    public function replycommentAction($slug='') {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['user_id'] = $user_session['id'];
            $postval['datetime'] = date("Y-m-d H:i:s");
            $lik = new BlogCommentReply();
            $lik->save($postval);
            return $response->redirect("index/comment/" . $slug . "/");
        }
    }

    public function likeblogAction() {
        $response = new \Phalcon\Http\Response();
        $user_session = $this->session->get("user");
        $postVal = $this->request->getPost();
        $like['id_blog'] = $postVal['id_blog'];
        $like['user_id'] = $user_session['id'];
        $like['status'] = 1;
        $likeinfo = BlogLike::findFirst(array("user_id='" . $like['user_id'] . "' AND id_blog='" . $like['id_blog'] . "' "));
        if (empty($likeinfo)) {
            $phql = "UPDATE MasterBlog SET totallike =totallike+1 where id_blog='{$like['id_blog']}'";
            $this->modelsManager->executeQuery($phql);
            $lik = new BlogLike();
            $lik->save($like);
        }
        echo 1;
        exit;
    }

    function aboutAction(){
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }

    function courseAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $course = MasterCourse::find("scid='".$postval['id']."'");
        foreach($course as $val){
            echo '<h4 onclick="select_course('.$val->id.')">'.$val->name.'</h4><br>';
        }
        exit();
    }

    function changecourseAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        if($this->request->isPost()){
            $postval=$this->request->getPost();
        }
        $course = $postval['course'];$sup_cat = $postval['sup_cat'];$id = $user_session['id'];
        $sql = "UPDATE  MasterUsers SET sup_cat='{$sup_cat}',course='{$course}'  WHERE uid='{$id}'";
        $status = $this->modelsManager->executeQuery($sql);
        $this->session->destroy();
        return $response->redirect("index/login/".$id);
        exit();
    }

    function studygroupAction() {
        $response = new \Phalcon\Http\Response();
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $id_user=$user_session['id'];
        //all invite friend in ischools user
        $invitedus = MasterFriendlist::find(array("LOCATE($id_user,accept_friend)"));
        $arr = array();
        foreach ($invitedus as $use) {
            $arr[] = $use->uid;
        }
        $alluinvfriend = MasterFriendlist::find(array("uid ='" . $user_session['id'] . "'"));
        $arrall = array();
        foreach ($alluinvfriend as $all) {
            $arrall = json_decode($all->accept_friend);
        }
        $all_invitedfirend = array_unique(array_merge($arr,$arrall));
        $friendid=implode(",",$all_invitedfirend);
        if($friendid==''){
            $user=array();
        }else{
            $user = MasterUsers::find(array("uid IN(" . $friendid . ")"));
        }
        $this->view->setVar("user", $user);

        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $this->view->setVar("course", $course);
        // Follow Action Here
        $follow = $this->followAction();
        $this->view->setVar("follow",$follow);
        $this->view->setVar("follower",$arr1);
        $this->view->setVar("following",$arr);

        // $subject = MasterSubject::findFirst(array("slug='".$slug."' AND status=1"));
    }

    function packageAction(){
        $response = new \Phalcon\Http\Response();
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $this->view->setVar("course", $course);

        $package = MasterPackages::find("sup_cat LIKE '%".$course->scid."%'");
        $this->view->setVar("package", $package);
    }

    function waletAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }else{
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='".$user_session['course']."'"));
        $this->view->setVar("course", $course);

        $points = MasterUserpoint::findFirst("uid='".$user_session['id']."'");
        $mup = MasterUserPackage::find("userid='{$user_session['id']}'");
        foreach($mup as $mval){
            if($mval->packageid!=''){
                $mvalue[] = $mval->packageid;
                $mupval[$mval->packageid] = ['test_type'=>$mval->test_type,'not'=>$mval->nots];
            }
        }
        $mvalue = implode(',', $mvalue);
        $package = MasterPackages::find("id IN (".$mvalue.")");
        $mt = MasterTests::find("status='1'");
        foreach($mt as $mtval){
            $tids[] = $mtval->id;
        }
        $tids = implode(",", $tids);
        $mr = MasterResult::find("testid IN ({$tids}) AND userid='{$user_session['id']}' ");

        $pup = UserPracticePackage::find("id_user='{$user_session['id']}'");
        foreach($pup as $pval){
            $pvalue[] = $pval->id_package;
        }
        $pvalue = implode(",", $pvalue);
        $pracpackage = MasterPracticePackage::find("id IN ('{$pvalue}')");

        $this->view->setVar("points", $points);
        $this->view->setVar("package", $package);
        $this->view->setVar("prcpackage", $pracpackage);
        $this->view->setVar("mupval", $mupval);
        $this->view->setVar("mt", $mt);
        $this->view->setVar("mr", $mr);
    }

    function quizAction()
    {
        $response = new \Phalcon\Http\Response();
        $date = date("Y-m-d");
//        $course = $this->request->getPost();
//        if(!$this->request->getPost('course')){
//            return $response->redirect("index");
//        }
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
            $quiz = MasterQuiz::findFirst("status='1' AND start_date<='{$date}' AND end_date>='{$date}' AND course='{$user_session['course']}'");
            if(!isset($quiz->status)){
                $quiz = MasterQuiz::findFirst("status='1' AND course='{$user_session['course']}' ORDER BY quiz_id DESC ");
            }
        }else{
            return $response->redirect("index");
        }
        $this->view->setVar("quiz", $quiz);
        $arr=json_decode($quiz->questionid);
        $rand_keys = array_rand($arr, $quiz->total);
        $questioid=array();
        $qnt=0;
        for($i=0;$i<$quiz->total;$i++){
            $questioid[]=$arr[$rand_keys[$i]];
            $qnt++;
        }
        $this->view->setVar("questionsIDlist",$questioid);
        $this->view->setVar("numqus",$qnt);
    }

    function followAction()
    {
        $array=[];
        $date = date("Y-m-d");
        $user_session = $this->session->get("user");
        $follow = MasterFollow::find("observe_date='{$date}' AND status='1' AND userid!='$user_session[id]'");
        foreach($follow as $fval){
            $array[] = $fval->userid;
        }
        
        $uid = implode("','", $array);
        $user = MasterUsers::find("uid IN ('{$uid}')");
        foreach($follow as $flval){
            foreach($user as $uval){
                if($uval->uid==$flval->userid){
                    $data[] = [
                    "userid"=>$flval->userid,
                    "img"=>$uval->img,
                    "name"=>$uval->first_name.' '.$uval->last_name,
                    "per"=>$flval->per
                    ];
                }
            }
        }
        return $data;
        exit();
    }
    
    function followusAction(){
        $response = new \Phalcon\Http\Response();
        $postVal = $this->request->getPost();
        $followid[]=$postVal['uid'];
        $user_session = $this->session->get("user");
        $chkuser = MasterFriendlist::findFirst(array("uid='" . $user_session['id'] . "'"));
        if ($chkuser->uid != '') {
            if ($chkuser->accept_friend != '') {
                $userall = json_decode($chkuser->accept_friend);
                $finalarr = array_merge((array) $userall, (array) $followid);
            } else {
                $finalarr = $followid;
            }
            $jsondata = json_encode(array_values($finalarr));
            $sql = "UPDATE  MasterFriendlist SET accept_friend = '$jsondata' WHERE uid='{$user_session['id']}'";
            $status = $this->modelsManager->executeQuery($sql);
        } else {
            $jsondata = json_encode(array_values($postdata['inviteuser']));
            $sql = "INSERT INTO  MasterFriendlist (accept_friend,uid) VALUES ('$jsondata','{$user_session['id']}')";
            $status = $this->modelsManager->executeQuery($sql);
        }
        echo 1;
        exit;
        
    }
    public function qodsolutionAction() {
        //its for show question of the day solution
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);

        $dt = date("Y-m-d");
        $qod = QuestionOfday::findFirst("show_date='" . $dt . "'");
        if (!isset($qod->id_qday)) {
            $qod = QuestionOfday::findFirst("questionid='QID00014'");
        }
        $this->view->setVar("qod", $qod);
    }
    public function faqAction(){
         $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
        $curs = MasterCourse::find(array("status=1"));
        $this->view->setVar("curs", $curs);
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }
    public function termconditionAction(){
        $setting =SiteSetting::findFirst();
        $this->view->setVar("setting", $setting);
    }
    
}