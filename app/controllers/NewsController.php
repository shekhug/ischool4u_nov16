<?php

class NewsController extends ControllerBase
{

	public function initialize() 
  {
    $this->view->setTemplateAfter('main');
    Phalcon\Tag::setTitle('Ischool4u | ADMIN');
    parent::initialize();
    if(!$this->session->has("admin"))
    {  
      header("location:".BASEURL.'admin/login');
    }
    $access = $this->session->get('admin');
    $this->view->setVar("access_roles", $access);
  }
    /**
     * News View Part Only retrive data from database and show the data.
     * @return [array] [Fetched Data]
     */
    function indexAction()
    {
    	$data = MasterNews::find();
      $this->view->setVar("new", $data);        
    }
    /**
    * Function To Add new News
    * Action Name: addpageAction
    * Created Date: 13-06-2015
    **/
    public function addnewsAction()
    {
      $response = new \Phalcon\Http\Response();
      if ($this->request->isPost()) {
        $postval=$this->request->getPost();
        $Addnew= new MasterNews();
        $Addnew->save($postval);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Newss Successfully Add</div>");
        return $response->redirect("news");

        
      }
    }

    /**
      * Function created for update status in News detail
      * Created Date: 13-06-2015
      */
    public function newupdstatusAction($u_status,$id)
    {
      $response = new \Phalcon\Http\Response();
      if($id!="")
      {
        if($u_status==2){
          $phql = "UPDATE MasterNews SET status = 0 where n_id=".$id."";
        }
        else
        {
          $phql = "UPDATE MasterNews SET status = 1 where n_id=".$id."";
        }
        $status = $this->modelsManager->executeQuery($phql);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>News Status Updated Successfully</div>");
        return $response->redirect("news");
      }
    }
     /**
     *function created for delete the News
     * Created Date: 13-06-2015
     */
     public function newdeleteAction($id)
     {
      $response = new \Phalcon\Http\Response();
      if($id!="")
      {
        $phql = "DELETE FROM MasterNews WHERE n_id = '".$id."'";
        $this->modelsManager->executeQuery($phql);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>News Deleted Successfully</div>");
        return $response->redirect("news");
      }
    }

    /**
     *function created for update the News
     * Created Date: 16-06-2015
     */
    public function newupdateAction($id){
      $response = new \Phalcon\Http\Response();
      if ($id!='') {
       
        $postval=$this->request->getPost();
        $conditions = "n_id = :n_id:";
        $parameters = array("n_id" => $id);
        $newu = MasterNews::findFirst(array($conditions,"bind" => $parameters));
        $this->view->setVar("bid", $newu);
        
      }
      if($this->request->isPost()){
        $postval = $this->request->getPost();
        $update= new MasterNews();
        $update->save($postval);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>News Successfully Update</div>");
        return $response->redirect("news");
      }
    }
     /**
     * Function created for slug value to check in database and send it status to add news
     * Created Date: 16-06-2015
     */

     static public function slugifyAction($text)
     { 
      if ($text=="") {
        echo $text="";exit();
      }
      else
      {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
          // trim
        $text = trim($text, '-');
          // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
          // lowercase
        $text = strtolower($text);
          // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text))
        {
          return 'n-a';
        }
        $next = MasterNews::find(array("slug = :slug:",'bind' => array('slug' => $text)));
        if (count($next)==1) {
          $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
          $status=0;
        }
        else
        {
          $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
          $status=1;
        }
        $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
        echo json_encode($slug_status); exit();
      }
    }

    


  }
  ?>