<?php
class AdminusersController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize(); 
        if(!$this->session->has("admin"))
        {  
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }
    public function indexAction(){
        //$this->view->setvar("magdate",'TRUE'); 
        
    	$user = MasterAdminUsers::find(array("order" => "first_name"));
        $this->view->setVar("user", $user);
    }
    public function addAction(){
    	if ($this->request->isPost()) {
            
            $postval=$this->request->getPost();
            $conditions = "mail_id = :mail_id:";
            $parameters = array("mail_id" => $this->request->getPost("mail_id"));
            $user = MasterAdminUsers::find(array($conditions,"bind" => $parameters));
            echo"<pre>";print_r($user);exit();
            if (count($user) != 0) {
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("mail_id")."has been already exist you are not allow to use this Email ID</div>");
                $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
            }
            else
            {
                $passsword=$this->request->getPost("pass");
                
                $totarr=array_merge($postval,array('pass'=>base64_encode($passsword),'created'=>date('y-m-d h:m:s')));
                if ($this->request->hasFiles() == true) {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        $path = rand(1, 1000) . $file->getName();
                        $newpath = 'author/' . $path;
                        $file->moveTo($newpath);
                        $totarr['profile_image']=$path;
                    }
                }
                $AdminUsers = new MasterAdminUsers();
                $AdminUsers->save($totarr);
                $getverify = MasterRole::findFirst(array("id='" . $postval['type'] . "' "));
                if($getverify->is_verifier==1){
                    $i=0;
                    foreach($postval['subject'] as $vn){
                        $veifUsers = new VerifierRole();
                        $vr['id_user']=$AdminUsers->aid;
                        $vr['id_subject']=$vn;
                        $vr['id_subsubject']=$postval['subsubject'][$i];
                        $vr['id_topic']=$postval['topic'][$i];
                        $vr['last_modified']=date("Y-m-d H:i:s");
                        $veifUsers->save($vr);
                        $i++;
                    }
                }
               $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Registered Successfully</div>");
                //$this->dispatcher->forward(array("action" => "index"));
                $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
            }
        }
        $role = MasterRole::find();
        $this->view->setVar("roles", $role);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    public function UpdateAction($id=''){
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $postval['modfied'] = date('y-m-d h:m:s');
            $postval['pass'] = base64_encode($postval['pass']);
            if ($this->request->hasFiles() == true) {
                    foreach ($this->request->getUploadedFiles() as $file) {
                        $path = rand(1, 1000) . $file->getName();
                        $newpath = 'author/' . $path;
                        $file->moveTo($newpath);
                        $postval['profile_image']=$path;
                    }
                }
            $AdminUsers = new MasterAdminUsers();
            $AdminUsers->save($postval);
            $getverify = MasterRole::findFirst(array("id='" . $postval['type'] . "' "));
                if($getverify->is_verifier==1){
                    $i=0;
                    foreach($postval['subject'] as $vn){
                        $veifUsers = new VerifierRole();
                        $vr['id_user']=$AdminUsers->aid;
                        $vr['id_subject']=$vn;
                        $vr['id_subsubject']=$postval['subsubject'][$i];
                        $vr['id_topic']=$postval['topic'][$i];
                        $vr['last_modified']=date("Y-m-d H:i:s");
                        $veifUsers->save($vr);
                        $i++;
                    }
                }else{
                       $phql1 = "DELETE FROM VerifierRole WHERE id_user = '".$postval['aid']."'";
                       $this->modelsManager->executeQuery($phql1); 
                }
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Admin User Updated Successfully</div>");
            //$this->dispatcher->forward(array("action" => "index"));
            return $response->redirect("adminusers");
        }
        $user = MasterAdminUsers::findFirst(array("aid='".$id."' "));
        $this->view->setVar("adminuser", $user);
        $getSubSub = MasterRole::findFirst(array("id='" . $user->type . "' "));
        $subrole=array();
        if($getSubSub->is_verifier==1){
            $subroled = VerifierRole::find(array("id_user='" . $id . "' "));
            foreach($subroled as $sr){
                $Sub = MasterSubject::findFirst(array("id='" . $sr->id_subject . "' "));
                $Subsub = MasterSubSubject::findFirst(array("ssid='" . $sr->id_subsubject . "' "));
                $Topic = MasterTopics::findFirst(array("tid='" . $sr->id_topic . "' "));
                $subrole[$sr->id]['subject']=$Sub->subjectname;
                $subrole[$sr->id]['subsubject']=$Subsub->ssname;;
                $subrole[$sr->id]['topic']=$Topic->tname;
            }
            $this->view->setVar("subrole",$subrole);
        }
        $role = MasterRole::find("");
        $this->view->setVar("roles",$role);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        
    }

    public function UpdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $date = date('y-m-d h:m:s');
                $phql = "UPDATE MasterAdminUsers SET is_active = 0 , modfied='{$date}' where aid=".$id."";
            }
            else
            {
                $date = date('y-m-d h:m:s');
                $phql = "UPDATE MasterAdminUsers SET is_active = 1 , modfied='{$date}' where aid=".$id."";
            }
            
                    //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

             $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Admin Status Updated Successfully</div>");
             //return $this->dispatcher->forward(array("controller"=>"adminusers","action" => "index"));
             return $response->redirect("adminusers");
        }
    }

    public function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
                $phql = "DELETE FROM MasterAdminUsers WHERE aid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $phql1 = "DELETE FROM VerifierRole WHERE id_user = '".$id."'";
                $this->modelsManager->executeQuery($phql1);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Deleted Successfully</div>");
                return $response->redirect("adminusers");
        }
        
    }
    public function addmoreAction(){
        $getSub = MasterSubject::find(array());
        $str="<option value=''>-- Choose Subject --</option>";
       foreach($getSub as $res){ 
       $str.='<option value="'.$res->id.'">'.$res->subjectname.'</option>';
         }
         $id=rand(1,333);
        echo '<div class="test'.$id.'"><div class="col-md-6 col-md-offset-3"><div class="row"><div class="col-md-4"><select name="subject[]" id="sub'.$id.'" class="form-control"  onchange="getsubsubject(this.value,'.$id.');">'.$str.'</select></div><div class="col-md-4"><select name="subsubject[]" id="subsub'.$id.'" class="form-control" onchange="gettopics(this.value,'.$id.');"><option value="">-- Choose Subsubject --</option></select></div><div class="col-md-4"><select name="topic[]" id="topc'.$id.'" class="form-control"><option value="">-- Choose Topics --</option></select></div></div></div><div class="col-md-3"><div class="row text-left add-text-field" style="margin-top: -30px;"><a href="#" onclick="addmorerow();"><i class="fa fa-plus-square text-success" aria-hidden="true"></i></a><a href="#" onclick="removerow('.$id.');"><i class="fa fa-minus-square text-danger" aria-hidden="true"></i></a></div></div></div>';
        exit;
    }
    public function checkverifyAction(){
        $getVal = $this->request->getPost();
        $getSubSub = MasterRole::findFirst(array("id='" . $getVal['id'] . "' "));
        echo $getSubSub->is_verifier;exit;
    }
    public function removeprevsubAction(){
        $getVal = $this->request->getPost();
        $phql1 = "DELETE FROM VerifierRole WHERE id = '".$getVal['id']."'";
        $this->modelsManager->executeQuery($phql1);
        echo 1;exit;
    }
}