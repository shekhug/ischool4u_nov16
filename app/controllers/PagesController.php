<?php
/**
* Pages Controller
* Controller Name: PagesController
* Created On:26/05/2015
* Modified: N/A
**/
class PagesController extends ControllerBase
{
  public function initialize(){
    $this->view->setTemplateAfter('main');
    Phalcon\Tag::setTitle('Ischool4u | ADMIN');
    parent::initialize();
    if(!$this->session->has("admin"))
    {
      header("location:".BASEURL.'admin/login');
    }
    $access = $this->session->get('admin');
    $this->view->setVar("access_roles", $access);
  }
 /**
* Pages list management Method/Action
* Method Name: indexAction 
* Created On:26/05/2015
* Modified: N/A
**/
public function indexAction(){ 
  $data = MasterCms::find();
  $this->view->setVar("pages", $data);
}
/**
* Function To Add new page
* Action Name: addpageAction
* Created Date: 26-05-2015
**/
public function addpageAction()
{
  $response = new \Phalcon\Http\Response();
  if ($this->request->isPost()) {
    $postval=$this->request->getPost();
    $page = MasterCms::find(array("page = :page:",'bind' => array('page' => $this->request->getPost("page"))));
    $slug = MasterCms::find(array("slug = :slug:",'bind' => array('slug' => $this->request->getPost("slug"))));
    if (count($page) != 0 ) {
      $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("page")." Page already exist</div>");
      return $response->redirect("pages");
    }
    else
    {
      if (count($slug) != 0 ) {
        $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("page")." as slug name already exist.</div>");
      }
      else
      {
        $Addcms= new MasterCms();
        $Addcms->save($postval);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Pages Successfully Add</div>");
        return $response->redirect("pages");
      }
    }
  }
}
/**
* Function To update changees in page
* Action Name: addpageAction
* Created Date: 26-05-2015
**/
public function updatepageAction($id){
  $response = new \Phalcon\Http\Response();
  if ($id!='') {
    $postval=$this->request->getPost();
    $conditions = "id = :id:";
    $parameters = array("id" => $id);
    $cms = MasterCms::find(array($conditions,"bind" => $parameters));
    $this->view->setVar("page", $cms);
         //exit();
  }
  if($this->request->isPost()){
    $postval = $this->request->getPost();
    $update= new MasterCms();
    $update->save($postval);
    $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Pages Successfully Update</div>");
    return $response->redirect("pages");
  }
}
public function cmsupdstatusAction($u_status,$id)
{
  $response = new \Phalcon\Http\Response();
  if($id!="")
  {
    if($u_status==2){
      $phql = "UPDATE MasterCms SET status = 0 where id=".$id."";
    }
    else
    {
      $phql = "UPDATE MasterCms SET status = 1 where id=".$id."";
    }
    $status = $this->modelsManager->executeQuery($phql);
    $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
      <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Pages Status Updated Successfully</div>");
    return $response->redirect("pages");
  }
}
    /**
     * @author preetish priyabrata
     *function created for delete the pages 
     */
    public function cmsdeleteAction($id)
    {
      $response = new \Phalcon\Http\Response();
      if($id!="")
      {
        $phql = "DELETE FROM MasterCms WHERE id = '".$id."'";
        $this->modelsManager->executeQuery($phql);
        $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Pages Deleted Successfully</div>");
        return $response->redirect("pages");
      }
    }
    /**
     * @author preetish priyabrata
     * Function created for slug value to check in database and send it status to add page
     */

    static public function slugifyAction($text)
    { 
      if ($text=="") {
        echo $text="";exit();
      }
      else
      {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
          // trim
        $text = trim($text, '-');
          // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
          // lowercase
        $text = strtolower($text);
          // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text))
        {
          return 'n-a';
        }
        $page = MasterCms::find(array("slug = :slug:",'bind' => array('slug' => $text)));
        if (count($page)!=0) {
          $msg="&nbsp;&nbsp;<span class='has-error'><label class='control-label' for='inputError'><i class='fa fa-times-circle-o'></i>&nbsp;Invalid Slug Name</label></span>";
          $status=0;
        }
        else
        {
          $msg="&nbsp;&nbsp;<span class='has-success'><label class='control-label' for='inputSuccess'><i class='fa fa-check'></i> Slug Name is Valid</label></span>";
          $status=1;
        }
        $slug_status=array("value"=>$text,"msg"=>$msg, "status"=>$status);
        echo json_encode($slug_status); exit();
      }
    }
  }