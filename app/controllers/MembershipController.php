<?php

class MembershipController extends ControllerBase
{
    public function initialize()
    {  
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Membership');
        parent::initialize();
        $ses_id=$this->session->get("authority");
        if($ses_id['id']!="")
        {  
        }
        else
        {
            return $this->dispatcher->forward(array(
                    "controller"=>"admin",
                    "action"=>"login"
                ));
        }
    }
    public function indexAction()
    {
        $ad = MasterMemberships::find(array("order" => "m_name"));
        $this->view->setVar("mem", $ad);
    }
     public function addAction()
    {
         if ($this->request->isPost()) {
            
            $postval=$this->request->getPost();
            $conditions = "m_name = :m_name:";
            $parameters = array("m_name" => $this->request->getPost("m_name"));
            $s_data = MasterMemberships::find(array($conditions,"bind" => $parameters));
            if (count($s_data) != 0) {
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("m_name")." already exists.</div>");
            }
            else
            {
                $AdminUsers = new MasterMemberships();
                $AdminUsers->save($postval);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Membership Created Successfully</div>");
                return $this->dispatcher->forward(array("controller"=>"membership","action" => "index"));
            }
            
        }       
    }
    //"b_img"=>$postval["b_img"],
    public function updateAction($id)
    {
       if($id!="")
        {
            if ($this->request->isPost()) {//echo $id;
                
                $postval=$this->request->getPost();
                $conditions = "m_name = :m_name:";
                $parameters = array("m_name" => $this->request->getPost("m_name"));
                $s_dt = MasterMemberships::find(array($conditions,"bind" => $parameters));
                foreach ($s_dt as $soc_key) {
                    $mid=$soc_key->mid;
                }
                //echo"<pre>";print_r($uid);exit();
                if (count($s_dt) != 0) {
                    if($mid==$id)
                    {
                        
                        $phql = "UPDATE MasterMemberships SET m_name = :m_name:,m_cost=:m_cost:,m_desc=:m_desc:,m_period=:m_period:,m_format=:m_format:,is_active = :is_active:,m_rec=:m_rec: WHERE mid = ".$id."";
                        //echo "<pre>";print_r($phql);
                        $status = $this->modelsManager->executeQuery($phql,$postval);
                        $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated Successfully</div>");
                        return $this->dispatcher->forward(array("controller"=>"membership","action" => "index"));
                    }
                    else
                    {
                       $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>".$this->request->getPost("m_name")." already exists.</div>");
                        
                    }
                    
                }
                else
                {
                    $phql = "UPDATE MasterMemberships SET m_name = :m_name:,m_cost=:m_cost:,m_desc=:m_desc:,m_period=:m_period:,m_format=:m_format:,is_active = :is_active:,m_rec=:m_rec: WHERE mid = ".$id."";
                    //echo "<pre>";print_r($phql);
                    $status = $this->modelsManager->executeQuery($phql,$postval);
                    $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated Successfully</div>");
                    return $this->dispatcher->forward(array("controller"=>"membership","action" => "index"));
                }
            }      
            //
            else
            {//echo $id+1;
                $soc_upd = MasterMemberships::findFirst(array("mid = :mid:",'bind' => array( 'mid' => $id)));
                $this->view->setVar("membership", $soc_upd);
            }
            
        }
    }
    
    public function UpdstatusAction($u_status,$id)
    {
       if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterMemberships SET is_active = 0 where mid=".$id."";
            }
            else
            {
                $phql = "UPDATE MasterMemberships SET is_active = 1 where mid=".$id."";    
            }
            
                    //echo "<pre>";print_r($phql);
            $status = $this->modelsManager->executeQuery($phql);

            $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Status Updated Successfully</div>");
             return $this->dispatcher->forward(array("controller"=>"membership","action" => "index"));
        }
    }
    public function deleteAction($id)
    {
       if($id!="")
        {
                $phql = "DELETE FROM MasterMemberships WHERE mid = '".$id."'";
                $this->modelsManager->executeQuery($phql);
                $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Deleted Successfully</div>");
                return $this->dispatcher->forward(array("controller"=>"membership","action" => "index"));
        }
    }
    
}
