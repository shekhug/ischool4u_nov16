<?php
class FixedtestController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if(!$this->session->has("admin"))
        {
            header("location:".BASEURL.'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    function indexAction()
    {
        $fixtest = MasterFixtest::find();
        $this->view->setVar("fixtest", $fixtest);
    }

    function addtestAction()
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFixtest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixtest Successfully Added</div>");
            return $response->redirect("fixedtest");
        }
    	$supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    function updateAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($this->request->isPost()){
            $postval=$this->request->getPost();
            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFixtest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixtest Updated Successfully </div>");
            return $response->redirect("fixedtest");
        }
        if($id!=''){
            $fixtest = MasterFixtest::findFirst(array("id='$id'"));
            $this->view->setVar("fixtest", $fixtest);
            $course = MasterCourse::find("scid='".$fixtest->sup_cat."'");
            $this->view->setVar("course", $course);
        }
        $supcat = MasterSupCat::find("status='1'");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
    }

    function updteststatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            if($u_status==2){
                $phql = "UPDATE MasterFixtest SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterFixtest SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Status Updated Successfully</div>");
            return $response->redirect("fixedtest");
        }
    }

    function deleteAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterFixtest WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixedtest Deleted Successfully</div>");
            return $response->redirect("fixedtest");
        }
    }

    function fixpackageAction()
    {
        $data = MasterPackages::find(array("test_type=4","order" => "id desc"));
        $this->view->setVar("package", $data);
    }

    function addpackagesAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            
                for ($i=0; $i <=count($postval['fixid'])-1 ; $i++) {
                    $fixedid[$i] = [
                        $postval['fixid'][$i],
                        $postval['date'][$i],
                        $postval['stime'][$i],
                        $postval['etime'][$i]
                    ];
                }
            
            $postval['fixtest_id'] = json_encode($fixedid);
            
            // echo '<pre>';print_r($postval);
            // exit();
            $package = new MasterPackages();
            $package->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fix Package Successfully Added</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
        $fixtest = MasterFixtest::find("course='".$package->course."'");
        $this->view->setVar("fixtest", $fixtest);
    }

    function packageupdstatusAction($u_status,$id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        { 
            if($u_status==2){
                $phql = "UPDATE MasterPackages SET status = 0 where id=".$id."";
            }else{
                $phql = "UPDATE MasterPackages SET status = 1 where id=".$id."";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixed Package Status Updated Successfully</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
    }

    function updatepackageAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if ($id!=''){
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $package = MasterPackages::findFirst(array($conditions,"bind" => $parameters));
            $this->view->setVar("val", $package);
            $supcat = MasterSupCat::find(array("status=1"));
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='".$package->sup_cat."'");
            $this->view->setVar("course", $course);
            $fixtest = MasterFixtest::find("sup_cat='".$package->sup_cat."' AND course='".$package->course."'");
            $this->view->setVar("fixtest", $fixtest);
        }
        if($this->request->isPost()){
            $postval = $this->request->getPost();
            $postval['not'] = null;
             for ($i=0; $i <=count($postval['fixid'])-1 ; $i++) {
                    $fixedid[$i] = [
                        $postval['fixid'][$i],
                        $postval['date'][$i],
                        $postval['stime'][$i],
                        $postval['etime'][$i]
                    ];
                }
            
            $postval['fixtest_id'] = json_encode($fixedid);
            $package = new MasterPackages();
            $package->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Updated</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
    }


    // Get question ids
    
    function getqidAction()
    {
    	if($this->request->isPost()){
            $postval = $this->request->getPost();
        }
        $qid = MasterQuestion::find("subject='".$postval['subjectid']."' AND subsubject='".$postval['ssubid']."' AND topics='".$postval['topics']."' AND subtopics='".$postval['subtopic']."'");
        ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Questionid</th>
                        <th>Question</th>
                        <th>Q. Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($qid as $val): ?>
                    <?php
                        if($val->tableid==1){
                            $question = MasterQuestionBank::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==2){
                            $question = MasterMatchQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==3){
                            $question = MasterNumericQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==4){
                            $question = MasterParaQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==5){
                            $question = MasterReasonQuestion::findFirst("questionid='".$val->questionid."'");
                        }elseif($val->tableid==6){
                            $question = MasterSubtheoQuestion::findFirst("questionid='".$val->questionid."'");
                        }
                        
                    ?>
                    <tr>
                        <td><?=$val->questionid?></td>
                        <td><?=$question->question?></td>
                        <td>
                        <?php
                            if($val->q_type==1){
                                echo 'Single';
                            }elseif($val->q_type==2){
                                echo 'Multiple';
                            }elseif($val->q_type==3){
                                echo 'Match Making';
                            }elseif($val->q_type==4){
                                echo 'Numeric';
                            }elseif($val->q_type==5){
                                echo 'Paragraph';
                            }elseif($val->q_type==7){
                                echo 'Reasoning';
                            }elseif($val->tableid==6){
                                echo 'Theory';
                            }elseif($val->tableid==8){
                                echo 'Subjective';
                            }
                        ?>
                        </td>
                        <td><div class="btn btn-primary" onclick="selectqid('<?=$val->questionid?>')">Add</div></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php
        exit();
    }

    function getfixtestAction()
    {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $fixtest = MasterFixtest::find("sup_cat='".$postval['sup_cat']."' AND course='".$postval['course']."'");
            if($fixtest->count()!=0){
                for($i=0;$i<=count($fixtest)-1;$i++){
                ?>

                    <div class="form-group">
                    <div class="col-md-3">
                        <select name="fixid[]" id="" class="form-control">
                            <option value="">-- Choose fixtest --</option>
                            <?php foreach($fixtest as $ft): ?>
                                <option value="<?=$ft->id?>"><?=$ft->name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="datepicker" name="date[]" class="form-control datepicker" placeholder="Starting Date">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="stime[]"  class="form-control timepick" placeholder="Starting Time">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="etime[]"  class="timepick form-control" placeholder="Ending Time">
                    </div>
                    </div>
                    <div class="clearfix"></div>
                <?php 
                }
            }else{
                echo 'No Fixtest are found.';
            }
        }
        exit();
    }

    public function deletepackagesAction($id)
    {
        $response = new \Phalcon\Http\Response();
        if($id!="")
        {
            $phql = "DELETE FROM MasterPackages WHERE id = '".$id."'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fix Package Deleted Successfully</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
    }
}
?>