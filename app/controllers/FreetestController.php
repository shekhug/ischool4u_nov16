<?php

class FreetestController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    function indexAction() {
        $freetest = MasterFreetest::find();
        $this->view->setVar("freetest", $freetest);
        $res = PreviousExamName::find(array("order" => "id desc"));
        $arr=array();
        foreach($res as $val){
            $arr[$val->id]=$val->name;
        }
        $this->view->setVar("res", $arr);
    }

    function addfreetestAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
//            $mft = MasterFreetest::findFirst("sup_cat='{$postval['sup_cat']}' AND course='{$postval['course']}' ");
//            
//            if(isset($mft->id)){
//                $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Free test on this course and category is present.</div>");
//                return $response->redirect("freetest");
//            }

            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFreetest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Free Test Successfully Added</div>");
            return $response->redirect("freetest");
        }
        $supcat = MasterSupCat::find("status=1");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $res = PreviousExamName::find(array("status=1", "order" => "name"));
        $this->view->setVar("res", $res);
    }

    function updateAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['questionid'] = json_encode($postval['questionid']);
            $fixtest = new MasterFreetest();
            $fixtest->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Freetest Successfully Updated</div>");
            return $response->redirect("freetest");
        }
        if ($id != '') {
            $fixtest = MasterFreetest::findFirst(array("id='$id'"));
            $this->view->setVar("fixtest", $fixtest);
            $course = MasterCourse::find("scid='" . $fixtest->sup_cat . "'");
            $this->view->setVar("course", $course);
        }
        $supcat = MasterSupCat::find("status='1'");
        $this->view->setVar("supcat", $supcat);
        $getSub = MasterSubject::find(array());
        $this->view->setVar("subdet", $getSub);
        $res = PreviousExamName::find(array("status=1", "order" => "name"));
        $this->view->setVar("res", $res);
    }

    function updteststatusAction($u_status, $id) {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            if ($u_status == 2) {
                $phql = "UPDATE MasterFixtest SET status = 0 where id=" . $id . "";
            } else {
                $phql = "UPDATE MasterFixtest SET status = 1 where id=" . $id . "";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Status Updated Successfully</div>");
            return $response->redirect("fixedtest");
        }
    }

    function deleteAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            $phql = "DELETE FROM MasterFreetest WHERE id = '" . $id . "'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Freetest Deleted Successfully</div>");
            return $response->redirect("freetest");
        }
    }

    function fixpackageAction() {
        $data = MasterPackages::find(array("test_type='4'"));
        $this->view->setVar("package", $data);
    }

    function addpackagesAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            if (count($postval['fixid']) == count($postval['month'])) {
                for ($i = 0; $i <= count($postval['fixid']) - 1; $i++) {
                    $postval['fixtest_id'][$i] = [
                    $postval['fixid'][$i],
                    $postval['month'][$i]
                    ];
                }
            }
            $postval['fixtest_id'] = json_encode($postval['fixtest_id']);
            $postval['period'] = null;
            $postval['format'] = null;
            $postval['not'] = null;
            // exit();
            $package = new MasterPackages();
            $package->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fix Package Successfully Added</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
        $supcat = MasterSupCat::find(array("status=1"));
        $this->view->setVar("supcat", $supcat);
    }

    function packageupdstatusAction($u_status, $id) {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            if ($u_status == 2) {
                $phql = "UPDATE MasterPackages SET status = 0 where id=" . $id . "";
            } else {
                $phql = "UPDATE MasterPackages SET status = 1 where id=" . $id . "";
            }
            $status = $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Fixed Package Status Updated Successfully</div>");
            return $response->redirect("fixedtest/fixpackage");
        }
    }

    function updatepackageAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($id != '') {
            $conditions = "id = :id:";
            $parameters = array("id" => $id);
            $package = MasterPackages::findFirst(array($conditions, "bind" => $parameters));
            $this->view->setVar("val", $package);
            $supcat = MasterSupCat::find(array("status=1"));
            $this->view->setVar("supcat", $supcat);
            $course = MasterCourse::find("scid='" . $package->sup_cat . "'");
            $this->view->setVar("course", $course);
        }
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['period'] = null;
            $postval['format'] = null;
            $postval['not'] = null;
            $updatepack = new MasterPackages();
            $updatepack->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Package Successfully Updated</div>");
            return $response->redirect("course/packages");
        }
    }

    // Get question ids

    function getqidAction() {
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
        }
        if ($postval['questionid'] != '') {
            $qid = MasterQuestion::find("questionid='" . $postval['questionid'] . "'");
        } else {
            $qid = MasterQuestion::find("subject='" . $postval['subjectid'] . "' AND subsubject='" . $postval['ssubid'] . "' AND topics='" . $postval['topics'] . "' AND subtopics='" . $postval['subtopic'] . "'");
        }
        ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Questionid</th>
                    <th>Question</th>
                    <th>Q. Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
        <?php foreach ($qid as $val): ?>
            <?php
            if ($val->tableid == 1) {
                $question = MasterQuestionBank::findFirst("questionid='" . $val->questionid . "'");
            } elseif ($val->tableid == 2) {
                $question = MasterMatchQuestion::findFirst("questionid='" . $val->questionid . "'");
            } elseif ($val->tableid == 3) {
                $question = MasterNumericQuestion::findFirst("questionid='" . $val->questionid . "'");
            } elseif ($val->tableid == 4) {
                $question = MasterParaQuestion::findFirst("questionid='" . $val->questionid . "'");
            } elseif ($val->tableid == 5) {
                $question = MasterReasonQuestion::findFirst("questionid='" . $val->questionid . "'");
            } elseif ($val->tableid == 6) {
                $question = MasterSubtheoQuestion::findFirst("questionid='" . $val->questionid . "'");
            }
            ?>
                    <tr>
                        <td><span onclick="showdeatils('<?= $val->questionid ?>');"><?= $val->questionid ?></td>
                        <td><?= $question->question ?></td>
                        <td>
                    <?php
                    if ($val->q_type == 1) {
                        echo 'Single';
                    } elseif ($val->q_type == 2) {
                        echo 'Multiple';
                    } elseif ($val->q_type == 3) {
                        echo 'Match Making';
                    } elseif ($val->q_type == 4) {
                        echo 'Numeric';
                    } elseif ($val->q_type == 5) {
                        echo 'Paragraph';
                    } elseif ($val->q_type == 7) {
                        echo 'Reasoning';
                    } elseif ($val->tableid == 6) {
                        echo 'Theory';
                    } elseif ($val->tableid == 8) {
                        echo 'Subjective';
                    }
                    ?>
                        </td>
                        <td><div class="btn btn-primary" onclick="selectqid('<?= $val->questionid ?>')">Add</div></td>
                    </tr>
                        <?php endforeach; ?>
            </tbody>
        </table>
                        <?php
                        exit();
                    }

                    function getfixtestAction() {
                        $response = new \Phalcon\Http\Response();
                        if ($this->request->isPost()) {
                            $postval = $this->request->getPost();
                            $fixtest = MasterFixtest::find("sup_cat='" . $postval['sup_cat'] . "' AND course='" . $postval['course'] . "'");
                            if ($fixtest->count() != 0) {
                                for ($i = 0; $i <= count($fixtest) - 1; $i++) {
                                    echo $i + 1;
                                    ?>

                    <div class="form-group">
                        <div class="col-md-3">
                            <select name="fixid[]" id="" class="form-control">
                                <option value="">-- Choose fixtest --</option>
                    <?php foreach ($fixtest as $ft): ?>
                                    <option value="<?= $ft->id ?>"><?= $ft->name ?></option>
                    <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="date[]" data-plugin-datepicker class="form-control" placeholder="Starting Date">
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="stime[]" data-plugin-timepicker class="form-control" placeholder="Starting Time">
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="etime[]" data-plugin-timepicker class="form-control" placeholder="Ending Time">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                                <?php
                            }
                        }else {
                            echo 'No Fixtest are found.';
                        }
                    }
                    exit();
                }

                /* Satya for change of Free Test */

                public function addexamAction() {
                    $response = new \Phalcon\Http\Response();
                    if ($this->request->isPost()) {
                        $postval = $this->request->getPost();
                        $postval['staus'] = $this->request->getPost();
                        $Addcourse = new PreviousExamName();
                        $Addcourse->save($postval);
                        return $response->redirect("freetest/addexam");
                    }
                    $res = PreviousExamName::find(array("order" => "id desc"));
                    $this->view->setVar("res", $res);
                }

                public function saveprvexamAction() {
                    $response = new \Phalcon\Http\Response();
                    if ($this->request->isPost()) {
                        $postval = $this->request->getPost();
                        $phql = "UPDATE PreviousExamName SET name ='" . $postval['name'] . "' where id=" . $postval['id'] . "";
                        $this->modelsManager->executeQuery($phql);
                        echo 1;
                        exit;
                    }
                }

                public function getdetailsAction() {
                    $response = new \Phalcon\Http\Response();
                    if ($this->request->isPost()) {
                        $postval = $this->request->getPost();
                        $questionsres = MasterQuestion::findFirst(array("questionid='" . $postval['curqid'] . "'"));
                        ?>
            <div class="panel panel-ssub">
                <div class="panel-body">
            <?php
            if ($questionsres->tableid == 1):
                $question = MasterQuestionBank::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' "));
                ?>
                        <div class="qextra clearfix">
                <?php
                $attempt = MasterQattempt::findFirst(array("questionid='" . $questionsres->questionid . "'"));
                if ($questionsres->q_type == 2) {
                    echo '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"> 1 | Multiple Type Questions</p>';
                } else {
                    echo '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"> 1 | Single Type Questions</p>';
                }
                ?>

                        </div>
                        <div class="qcontent" data-ans="<?= $question->ans ?>">
                            <?= $question->question ?>                                                        
                        </div>
                            <?php if ($questionsres->q_type == 2) { ?>
                            <ul class="list-group">
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">A</span> <?= $question->option1 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">B</span> <?= $question->option2 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">C</span> <?= $question->option3 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">D</span> <?= $question->option4 ?></li>
                            </ul>
                <?php } else { ?>
                            <ul class="list-group">
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">A</span> <?= $question->option1 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">B</span> <?= $question->option2 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">C</span> <?= $question->option3 ?></li>
                                <li class="list-group-item"><span class="badge badge-inverse pull-left">D</span> <?= $question->option4 ?></li>
                            </ul>
                <?php } ?>

            <?php elseif ($questionsres->tableid == 2):
                $question = MasterMatchQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                        <div class="qextra clearfix">
                <?= '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"> 1 | Match Type Questions</p>' ?>

                        </div>
                        <div class="qcontent" data-ans="<?= $question->ans ?>">
                <?= $question->question ?>
                        </div>
                        <table class="table table-bordered match-table">
                            <tbody>
                                <tr>
                                    <td><span>A</span> <?= $question->a ?></td>
                                    <td><span>P</span> <?= $question->p ?></td>
                                </tr>
                                <tr>
                                    <td><span>B</span> <?= $question->b ?></td>
                                    <td><span>Q</span> <?= $question->q ?></td>
                                </tr>
                                <tr>
                                    <td><span>C</span> <?= $question->c ?></td>
                                    <td><span>R</span> <?= $question->r ?></td>
                                </tr>
                                <tr>
                                    <td><span>D</span> <?= $question->d ?></td>
                                    <td><span>S</span> <?= $question->s ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="qoption">
                            <div class="row">
                                <div class="col-md-12">
                <?php
                $aans = array("A", "B", "C", "D", "E");
                if ($question->ans_e == '') {
                    $count = 3;
                } else {
                    $count = 4;
                }
                ?>
                <?php for ($i = 0; $i <= $count; $i++): ?>
                                        <div class="col-md-1"><?= $aans[$i] ?>:</div>
                                        <div class="col-md-11">
                                            <div class="checkbox-custom checkbox-default"><input type="checkbox" value="P" name="<?= $question->questionid ?>ans<?= strtolower($aans[$i]) ?>p"><label>P</label></div>
                                            <div class="checkbox-custom checkbox-default"><input type="checkbox" value="Q" name="<?= $question->questionid ?>ans<?= strtolower($aans[$i]) ?>q"><label>Q</label></div>
                                            <div class="checkbox-custom checkbox-default"><input type="checkbox" value="R" name="<?= $question->questionid ?>ans<?= strtolower($aans[$i]) ?>r"><label>R</label></div>
                                            <div class="checkbox-custom checkbox-default"><input type="checkbox" value="S" name="<?= $question->questionid ?>ans<?= strtolower($aans[$i]) ?>s"><label>S</label></div>
                                        <?php if ($question->ans_e != ""): ?>
                                                <div class="checkbox-custom checkbox-default"><input type="checkbox" value="T" name="<?= $question->questionid ?>ans<?= strtolower($aans[$i]) ?>t"><label>T</label></div>
                                        <?php endif; ?>
                                        </div>
                                        <div class="clearfix"></div>
                <?php endfor; ?>
                                </div>
                            </div>
                        </div>
                                    <?php elseif ($questionsres->tableid == 3):
                                        $question = MasterNumericQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                        <div class="qextra clearfix">
                                        <?= '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"> 1 | Numeric Type Questions</p>' ?>

                        </div>
                        <div class="qcontent" data-ans="<?= $question->ans ?>">
                <?= $question->question ?>
                        </div>
                        <div class="qoption">
                            <div class="row">
                                <div class="col-md-3">Min:  <?= $question->option1 ?></div>
                                <div class="col-md-3" >Max:  <?= $question->option2 ?></div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="<?= $question->questionid ?>" placeholder="Answer">
                                </div>
                            </div>
                        </div>
            <?php elseif ($questionsres->tableid == 4):
                $question = MasterParaQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                        <div class="qextra clearfix">
                <?= '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"> 1 | Paragraph Type Questions</p>' ?>

                        </div>
                        <div class="qcontent">
                <?= $question->para ?>
                        </div>
                        <?php $opt = 1;
                        for ($i = 1; $i <= 4; $i++): ?>
                            <div class="para-opt">
                                <?php
                                if ($i == 1 && $question->question1 != '') {
                                    echo $question->question1;
                                } elseif ($i == 2 && $question->question2 != '') {
                                    echo $question->question2;
                                } elseif ($i == 3 && $question->question3 != '') {
                                    echo $question->question3;
                                } elseif ($i == 4 && $question->question4 != '') {
                                    echo $question->question4;
                                }
                                ?>
                            </div>
                            <div class="qoption">
                                <div class="">
                                <?php if ($i == 1 && $question->question1 != ''): ?>
                                        <ul class="list-group">
                                            <li class="list-group-item"><?= $question->option1 ?></li>
                                            <li class="list-group-item"><?= $question->option2 ?></li>
                                            <li class="list-group-item"><?= $question->option3 ?></li>
                                            <li class="list-group-item"><?= $question->option4 ?></li>
                                        </ul>
                    <?php elseif ($i == 2 && $question->question2 != ''): ?>
                                        <ul class="list-group">
                                            <li class="list-group-item"><?= $question->option5 ?></li>
                                            <li class="list-group-item"><?= $question->option6 ?></li>
                                            <li class="list-group-item"><?= $question->option7 ?></li>
                                            <li class="list-group-item"><?= $question->option8 ?></li>
                                        </ul>
                    <?php elseif ($i == 3 && $question->question3 != ''): ?>
                                        <ul class="list-group">
                                            <li class="list-group-item"><?= $question->option9 ?></li>
                                            <li class="list-group-item"><?= $question->option10 ?></li>
                                            <li class="list-group-item"><?= $question->option11 ?></li>
                                            <li class="list-group-item"><?= $question->option12 ?></li>
                                        </ul>
                    <?php elseif ($i == 4 && $question->question4 != ''): ?>
                                        <ul class="list-group">
                                            <li class="list-group-item"><?= $question->option13 ?></li>
                                            <li class="list-group-item"><?= $question->option14 ?></li>
                                            <li class="list-group-item"><?= $question->option15 ?></li>
                                            <li class="list-group-item"><?= $question->option16 ?></li>
                                        </ul>
                    <?php endif; ?>
                                </div>
                            </div>
                <?php endfor; ?>
            <?php elseif ($questionsres->tableid == 5):
                $question = MasterReasonQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' ")); ?>
                        <div class="qextra clearfix">
                <?= '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"><span class=""> 1 </span> | Reason Assertion</p>' ?>

                        </div>
                        <div class="qcontent" data-ans="<?= $question->ans ?>">
                        <?= $question->question ?>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item"><span class="badge badge-inverse pull-left">A</span> <?= $question->option1 ?></li>
                            <li class="list-group-item"><span class="badge badge-inverse pull-left">B</span> <?= $question->option2 ?></li>
                            <li class="list-group-item"><span class="badge badge-inverse pull-left">C</span> <?= $question->option3 ?></li>
                            <li class="list-group-item"><span class="badge badge-inverse pull-left">D</span> <?= $question->option4 ?></li>
                        </ul>
                        <?php
                        elseif ($questionsres->tableid == 6):
                            $question = MasterSubtheoQuestion::findFirst(array("questionid='" . $questionsres->questionid . "' AND qid='" . $questionsres->qusid . "' "));
                            $mquestion = MasterQuestion::findFirst("questionid='" . $question->questionid . "'");
                            ?>
                        <div class="top-menus clearfix">
                <?php if ($mquestion->q_type == 6) { ?>
                                echo '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"><b class="badge bg-default"> 1 </b> | Theory Type Questions</p>';
                        <?php } else { ?>
                                echo '<p class="text-uppercase pull-left unattempt" id="' . $question->questionid . 'aoua"><b class="badge bg-default"> 1 </b> | Subjective Type Questions</p>';
                        <?php } ?>
                        </div>
                        <div class="qcontent" data-ans="">
                            <?= $question->question ?>
                        </div>
                        <?php endif; ?>
                </div>
            </div>
            <div class="col-md-12 text-center">
            <?php if ($postval['flag'] == 1) { ?>
                    <input type="button" onclick="removeques('<?= $postval['curqid'] ?>');" class="btn btn-success text-center" value="Remove">
                    <button type="button" class="btn btn-success text-center" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <?php } else { ?>
                    <button type="button" class="btn btn-success text-center" data-dismiss="modal" aria-label="Close">Close</button>    
            <?php } ?>
            </div>

            <?php
            }
        }

    }
    ?>