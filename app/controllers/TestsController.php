<?php

/**
 * Pages Controller
 * Controller Name: Test Controller
 * Author By: Rajesh
 * Created On:8/08/2015
 * Modified: N/A
 * */
class TestsController extends ControllerBase {

    public function initialize() {
        date_default_timezone_set('Asia/Calcutta');
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
    }

    function indexAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        error_reporting(0);
        if ($this->request->isGet()) {
            $getVal = $this->request->get();
            if ($getVal['submit']) {
                if ($getVal['subject'] == '') {
                    unset($getVal['subject']);
                } else {
                    $where = ' subject="' . $getVal['subject'] . '"';
                    $subsubdet = MasterSubSubject::find("subid = '" . $getVal['subject'] . "' AND status=1");
                    $this->view->setVar("subsubdet", $subsubdet);
                };
                if ($getVal['subsubject'] == '') {
                    unset($getVal['subsubject']);
                } else {
                    $where .= ' AND subsubject ="' . $getVal['subsubject'] . '"';
                    $mTopicdet = MasterTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND status=1");
                    $this->view->setVar("mTopicdet", $mTopicdet);
                };
                if ($getVal['topics'] == '') {
                    unset($getVal['topics']);
                } else {
                    $where .= ' AND topics="' . $getVal['topics'] . '"';
                    $msTopicdet = MasterSubTopics::find("subid = '" . $getVal['subject'] . "' AND ssubid='" . $getVal['subsubject'] . "' AND tid='" . $getVal['topics'] . "' AND status=1");
                    $this->view->setVar("msTopicdet", $msTopicdet);
                };
                if ($getVal['subtopics'] == '') {
                    unset($getVal['subtopics']);
                } else {
                    $where .= ' AND subtopics="' . $getVal['subtopics'] . '"';
                };
                $arr = array($where);
            } else {
                $arr = array();
            }
            $tests = MasterTests::find($arr);
            $this->view->setVar("tests", $tests);
        }
    }

    function addtestsAction($test_type='', $exam_type='', $courseid='') {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['sort'] = json_encode($postval['sort']);
            $extyid = $postval['exam_type'];
            $postval['qint'] = json_encode($postval['qint']);
            $Addques = new MasterTests();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Test Successfully Add</div>");
            return $response->redirect("exams");
        }
        $supcat = MasterSupCat::find("status=1");
        $supca = array();
        foreach ($supcat as $v) {
            $supca[$v->scid] = $v->scname;
        }
        $this->view->setVar("supcat", $supca);
        $course = MasterCourse::find("status=1");
        $couca = array();
        foreach ($course as $v) {
            $couca[$v->id] = $v->name;
        }
        $this->view->setVar("course", $couca);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $qtype);
        $this->view->setVar("test_type", $test_type);
        $this->view->setVar("exam_type", $exam_type);
        $this->view->setVar("couse", $courseid);
    }

    function deletetestAction($id='') {
        $response = new \Phalcon\Http\Response();
        if ($id != "") {
            $phql = "DELETE FROM MasterTests WHERE id = '{$id}'";
            $this->modelsManager->executeQuery($phql);
            $this->flashSession->success(" <div class='alert alert-warning alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Test Successfully Deleted</div>");
            return $response->redirect("tests");
        }
    }

    function unitAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }

        // print_r($user_session);exit();
    }

    function addunitAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $course = MasterCourse::find("status=1");
        $this->view->setVar("course", $course);
        $getSub = MasterSubject::find(array("status=1"));
        $this->view->setVar("subdet", $getSub);
    }

    function examtAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        if ($this->request->isPost()) {
            $getVal = $this->request->getPost();
            $test_type = $getVal['test_type'];
            $exam_type = MasterExamType::find(array("exam_type='" . $test_type . "' AND status='1'"));
            echo "<option value=''>-- Select Test --</option>";
            foreach ($exam_type as $value) {
                echo "<option value='" . $value->etid . "'>" . $value->etname . "</option>";
            }
        } else {
            echo 'No Post data';
        }
        exit();
    }

    function testcurAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        if ($this->request->isPost()) {
            $getVal = $this->request->getPost();
            $etid = $getVal['etid'];
            $exam_type = MasterExamType::findFirst(array("etid='" . $etid . "'"));
            $examcourse = json_decode($exam_type->course);
            $course = MasterCourse::find(array("status=1"));
            echo "<option value=''>-- Select Course --</option>";
            foreach ($course as $value) {
                foreach ($examcourse as $val) {
                    if ($value->id == $val) {
                        echo "<option value='" . $value->id . "'>" . $value->name . "</option>";
                    }
                }
            }
        } else {
            echo 'No Post data';
        }
        exit();
    }

    function stuwiseAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);

        if ($this->request->isPost()) {
            $getVal = $this->request->getPost();
            if (empty($getVal['course'])) {
                echo 'Choose a Course';
                exit();
            }
            $course = $getVal['course'];
            $test_type = $getVal['test_type'];
            $etid = $getVal['etid'];
            $subjects = MasterSubject::find(array("status='1'"));
            $q_type = MasterExamType::findFirst(array("etid='" . $etid . "'"));
            $neq_qtype = $q_type;
            $ques_type = MasterQuesType::find("status=1");
            $q_type = json_decode($q_type->q_type);
            if ($test_type == 1) {
                $subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
                $subject = json_decode($subject->subject);
                ?>
                <div class="form-group">
                    <div class="col-md-3 text-right">
                        <label class="text-right" style="font-size: 15px;font-weight: 600;">Choose Subject:</label>
                    </div>
                    <div class="col-md-9 text-left">
                        <?php
                        if (!empty($subject)) {
                            foreach ($subject as $value) {
                                foreach ($subjects as $val) {
                                    if ($val->id == $value) {
                                        ?>
                                        <label style="padding-left:10px;">
                                            <div class="radio-custom radio-default">
                                                <input type="radio" required class="chkcnt" id="curs<?= $value ?>" value="<?= $value ?>" name="sort[]">
                                                <label for="curs<?= $value ?>" ><?= $val->subjectname ?></label>
                                            </div>
                                        </label>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            echo 'No subject assign to this course. Please check the course Module.';
                        }
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="addtest-fixed-data"><p>Question in a Test: <span id="no_que"> <?= $neq_qtype->no_qs ?></span><p> Time: <span> <?= $neq_qtype->time ?> min</span></p></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?php
                        foreach ($ques_type as $value) {
                            foreach ($q_type as $val) {
                                if ($value->qtid == $val) {
                                    ?>
                                    <div class="qs-type-con">
                                        <input type="hidden" name="qint<?= $val ?>[]" value="<?= $val ?>" class="form-control">
                                        <div class="qs-type-list">
                                            <h5 class="qs-type-heading"><?= $value->qtname ?> <span id="qt<?= $value->qtid ?>"></span></h5>
                                            <div class="col-md-6">
                                                <label for="">Max Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Min Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            } elseif ($test_type == 2) {
                $subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
                $subject = json_decode($subject->subject);
                ?>
                <div class="form-group">
                    <h3 class="choose-topics-h">Choose Topics <i class="fa fa-hand-o-down" aria-hidden="true"></i></h3>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <table class="table table-bordered choose-topics-tbl">
                            <tr>
                                <?php
                                if (!empty($subject)) {
                                    foreach ($subject as $value) {
                                        foreach ($subjects as $val) {
                                            if ($val->id == $value) {
                                                ?>
                                                <td>
                                                    <label for="" class="bg-success main-head"><?= $val->subjectname ?></label>
                                                    <div class="clearfix"></div>
                                                    <?php $subsubject = MasterSubSubject::find(array("subid='$val->id' AND status=1")); ?>
                                                    <?php
                                                    foreach ($subsubject as $ssres) {
                                                        $topics = MasterTopics::find(array("subid='" . $val->id . "' AND ssubid='" . $ssres->ssid . "' AND status=1"));
                                                        if ($topics->count()) {
                                                            echo '<div class="choose-topics-content"><p>' . $ssres->ssname . '</p><hr>';
                                                            echo '<div class="clearfix"></div>';
                                                            foreach ($topics as $value) {
                                                                ?>
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input type="checkbox" id="curs<?= $value->tid ?>" value="<?= $value->tid ?>" name="sort[]" class="chkcnt">
                                                                    <label for="curs<?= $value->tid ?>" ><?= $value->tname ?></label>
                                                                </div>
                                                                <div class="clearfix"></div>

                                                                <?php
                                                            }
                                                            echo '</div>';
                                                        }
                                                    }
                                                    echo '<div class="clearfix"></div>';
                                                    ?>
                                                    <?php
                                                }
                                            }
                                        }
                                    } else {
                                        echo 'No subject assign to this course. Please check the course Module.';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="addtest-fixed-data"><p>Question in a Test: <span id="no_que"> <?= $neq_qtype->no_qs ?> </span></p><p> Time : <span>: <?= $neq_qtype->time ?> min</span></p></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?php
                        foreach ($ques_type as $value) {
                            foreach ($q_type as $val) {
                                if ($value->qtid == $val) {
                                    ?>
                                    <div class="qs-type-con">
                                        <input type="hidden" name="qint<?= $val ?>[]" value="<?= $val ?>" class="form-control">
                                        <div class="qs-type-list">
                                            <h5 class="qs-type-heading"><?= $value->qtname ?><span id="qt<?= $value->qtid ?>"></span></h5>        
                                            <div class="col-md-6">
                                                <label for="">Max Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control" >
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Negetive Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            } elseif ($test_type == 3) {
                $subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
                $subject = json_decode($subject->subject);
                ?>
                <div class="form-group">
                    <div class="col-md-3 text-right">
                        <label class="text-right" style="font-size: 15px;font-weight: 600;">Choose Subject:</label>
                    </div>
                    <table class="table table-bordered choose-topics-tbl">
                        <tr>
                            <?php
                            if (!empty($subject)) {
                                foreach ($subject as $value) {
                                    foreach ($subjects as $val) {
                                        if ($val->id == $value) {
                                            ?>
                                        <label style="padding-left:10px;">
                                            <div class="radio-custom radio-default">
                                                <input type="radio" required class="chkcnt" id="cursda<?= $value ?>" value="<?= $value ?>" onclick="getalltopic(this.value);" name="subjectall">
                                                <label for="cursda<?= $value ?>" ><?= $val->subjectname ?></label>
                                            </div>
                                        </label>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            echo 'No subject assign to this course. Please check the course Module.';
                        }
                        ?>
                        </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group topic">

                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Unit name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="unit">
                    </div>
                </div>
                <div class="form-group">
                    <div class="addtest-fixed-data"><p> Question in a Test: <span id="no_que"> <?= $neq_qtype->no_qs ?></span></p><p> Time : <span>: <?= $neq_qtype->time ?> min</span></p></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?php
                        foreach ($ques_type as $value) {
                            foreach ($q_type as $val) {
                                if ($value->qtid == $val) {
                                    ?>
                                    <div class="qs-type-con">
                                        <input type="hidden" name="qint<?= $val ?>[]" value="<?= $val ?>" class="form-control">
                                        <div class="qs-type-list">
                                            <h5 class="qs-type-heading"><?= $value->qtname ?><span id="qt<?= $value->qtid ?>"></span></h5>
                                            <div class="col-md-6">
                                                <label for="">Max Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control" >
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Negative Mark</label>
                                                <input type="text" name="qint[<?= $val ?>][]" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            } elseif ($test_type == 5) {
                $subject = MasterCourse::findFirst(array("id='$course' AND status=1"));
                $subject = json_decode($subject->subject);
                ?>
                <div class="form-group">
                    <div class="col-md-3 text-right">
                        <label class="text-right" style="font-size: 15px;font-weight: 600;">Choose Subject:</label>
                    </div>
                    <div class="col-md-9 text-left">
                        <?php
                        if (!empty($subject)) {
                            foreach ($subject as $value) {
                                foreach ($subjects as $val) {
                                    if ($val->id == $value) {
                                        ?>
                                        <div class="checkbox-custom checkbox-default">
                                            <input type="checkbox" id="curs<?= $value ?>" value="<?= $value ?>" name="sort[]" class="chkcnt" onclick="countques(this.value);">
                                            <label for="curs<?= $value ?>" ><?= $val->subjectname ?></label>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                        } else {
                            echo 'No subject assign to this course. Please check the course Module.';
                        }
                        ?>
                    </div>
                </div>
                <hr style="opacity:0;">
                <div class="form-group">
                    <div class="addtest-fixed-data"><p> Question in a Test: <span id="no_que"> <?= $neq_qtype->no_qs ?> </span></p><p> Time : <span>: <?= $neq_qtype->time ?> min</span></p></div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <?php
                        foreach ($ques_type as $value) {
                            foreach ($q_type as $val) {
                                if ($value->qtid == $val) {
                                    ?>
                                    <div class="qs-type-con">
                                        <input type="hidden" name="qint<?= $val ?>[]" value="<?= $val ?>" class="form-control">
                                        <div class="qs-type-list">
                                            <h5 class="qs-type-heading"><?= $value->qtname ?><span id="qt<?= $value->qtid ?>"></span></h5>

                                            <div class="col-md-6">
                                                <label for="">Max Mark</label>
                                                <input type="text" name="qint<?= $val ?>[]" class="form-control" >
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Negetive Mark</label>
                                                <input type="text" name="qint<?= $val ?>[]" class="form-control" >
                                            </div>
                                        </div>
                                    </div>                        
                                    <div class="clearfix"></div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
        } else {
            echo 'No Post data';
        }
        exit();
    }

    function subjectwiseAction($test_id='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($test_id == '') {
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='1'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("id='" . $test_id . "'"));
        $exam_id = MasterExamType::findFirst(array("etid='" . $master_exam->exam_type . "'"));
        $this->view->setVar("master_exam", $master_exam);
        $subjects = json_decode($master_exam->sort);
        $q_type = json_decode($master_exam->qint);
        $subject = MasterSubject::findFirst("slug='" . strtolower($subject) . "'");
        $qcount = 0;
        foreach ($q_type as $key => $va) {
            $topicwise = MasterTestExam::find(array("id_test=$test_id"));
            foreach ($topicwise as $valtopic) {
                $used = json_decode($valtopic->qint);
                $totn[$key][] = $used->$key;
                foreach ($totn[$key] as $vanew) {
                    $easy = $vanew[1];
                    $medium = $vanew[2];
                    $hard = $vanew[3];
                }
                $level[1] = $easy ? $easy : 0;
                $level[2] = $medium ? $medium : 0;
                $level[3] = $hard ? $hard : 0;
                for ($i = 1; $i <= 3; $i++) {
                    $qus = MasterQuestion::find(array("topics='" . $valtopic->topic . "' AND q_type='" . $key . "' AND q_level='{$i}' AND e_type LIKE '%2%' ORDER BY rand() LIMIT " . $level[$i] . "  "));
                    if (!empty($qus)) {
                        foreach ($qus as $val) {
                            $questions[$key][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                }
            }
        }
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("qwttype", $qwttype);
        $this->view->setVar("etype", $exam_id);
        $this->view->setVar("subject", $subject);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function topicwiseAction($test_id='', $topic='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($test_id == '' || $topic == '') {
            return $response->redirect("index/dashboard");
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='2'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $course = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("id='" . $test_id . "'"));
        $exam_id = MasterExamType::findFirst(array("etid='" . $master_exam->exam_type . "' AND sup_cat='" . $sup_cat . "' AND course LIKE '%{$course}%'"));
        $this->view->setVar("master_exam", $master_exam);
        $q_type = json_decode($master_exam->qint);
        $qcount = 0;
        foreach ($q_type as $key => $va) {
            $topicwise = MasterTestExam::find(array("id_test=$test_id AND topic=$topic"));
            foreach ($topicwise as $valtopic) {
                $used = json_decode($valtopic->qint);
                $totn[$key][] = $used->$key;
                foreach ($totn[$key] as $vanew) {
                    $easy = $vanew[1];
                    $medium = $vanew[2];
                    $hard = $vanew[3];
                }
                $level[1] = $easy ? $easy : 0;
                $level[2] = $medium ? $medium : 0;
                $level[3] = $hard ? $hard : 0;
                for ($i = 1; $i <= 3; $i++) {
                    $qus = MasterQuestion::find(array("topics='" . $valtopic->topic . "' AND q_type='" . $key . "' AND q_level='{$i}' AND e_type LIKE '%2%' ORDER BY rand() LIMIT " . $level[$i] . "  "));
                    if (!empty($qus)) {
                        foreach ($qus as $val) {
                            $questions[$key][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                }
            }
        }
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype", $exam_id);
        $topic = MasterTopics::findFirst("tid='" . $topic . "'");
        $this->view->setVar("topic", $topic);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function unitwiseAction($id='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='3'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $test = MasterTests::findFirst("id='" . $id . "'");
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("etid='" . $test->exam_type . "' AND sup_cat='" . $sup_cat . "'"));
        $qdetail = json_decode($test->qint);
        $qcount = 0;
        foreach ($qdetail as $key => $va) {
            $topicwise = MasterTestExam::find(array("id_test=$id"));
            foreach ($topicwise as $valtopic) {
                $used = json_decode($valtopic->qint);
                $totn[$key][] = $used->$key;
                foreach ($totn[$key] as $vanew) {
                    $easy = $vanew[1];
                    $medium = $vanew[2];
                    $hard = $vanew[3];
                }
                $level[1] = $easy ? $easy : 0;
                $level[2] = $medium ? $medium : 0;
                $level[3] = $hard ? $hard : 0;
                for ($i = 1; $i <= 3; $i++) {
                    $qus = MasterQuestion::find(array("topics='" . $valtopic->topic . "' AND q_type='" . $key . "' AND q_level='{$i}' AND e_type LIKE '%2%' ORDER BY rand() LIMIT " . $level[$i] . "  "));
                    if (!empty($qus)) {
                        foreach ($qus as $val) {
                            $questions[$key][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                        }
                    }
                }
            }
        }
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype", $exam_id);
        $this->view->setVar("unit", $test->unit);
        $this->view->setVar("unitid", $test->id);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function fixedtestAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='4'");
        if (!isset($package->id)) {
            return $response->redirect("index/dashboard");
        }
        $fixtest = MasterFixtest::findFirst("id='" . $id . "'");
        $questions = json_decode($fixtest->questionid);
        $qid = implode("','", $questions);
        $qtype = MasterQuesType::find("status='1'");
        $qcount = 0;

        foreach ($qtype as $qvalue) {
            $qus = MasterQuestion::find(array("q_type='" . $qvalue->qtid . "' AND questionid IN ('{$qid}') "));
            if ($qus->count() != 0) {
                foreach ($qus as $val) {
                    $question[$qvalue->qtid][] = $val->questionid;
                    $qcount++;
                    $qids[] = $val->questionid;
                }
            }
        }
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("questionsIDlist", $question);
        $this->view->setVar("fixtest", $fixtest);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
    }

    function freetestsAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $freetest = MasterFreetest::find("sup_cat='{$user_session['sup_cat']}' AND status=1");
        $i = 0;
        $ar = array();
        foreach ($freetest as $frtst) {
            $ar[$frtst->prev_type][$i]['name'] = $frtst->name;
            $ar[$frtst->prev_type][$i]['id'] = $frtst->id;
            $i++;
        }
        $res = PreviousExamName::find(array("order" => "id asc", "status" => 1));
        $this->view->setVar("res", $res);
        $this->view->setVar("freetest", $ar);
        $this->view->setVar("course", $course);
    }

    function freetestAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }

        $freetest = MasterFreetest::findFirst("id='{$id}'");
        $questions = json_decode($freetest->questionid);
        $qid = implode("','", $questions);
        $qtype = MasterQuesType::find("status='1'");
        $qcount = 0;
        $getVal=$this->request->get();
        if($getVal['qtype']!=''){
            $str=str_split($getVal['qtype'],1);
        }
         $cond=" AND questionid IN ('{$qid}')";
         if($getVal['qlavel']!=''){
            $cond.=" AND q_level=".$getVal['qlavel'];
         }
         if($getVal['atp']!=''){
            $cond.=" AND subject=".$getVal['atp'];
         }
        foreach ($qtype as $qvalue) {
            if($getVal['qtype']!='' && in_array($qvalue->qtid, $str)){
                $qus = MasterQuestion::find(array("q_type='" . $qvalue->qtid . "'".$cond));
            }
            if($getVal['qtype']==''){
                $qus = MasterQuestion::find(array("q_type='" . $qvalue->qtid . "'".$cond)); 
            }
            $countques=isset($qus)?$qus->count():0;
            if ($countques != 0) {
                foreach ($qus as $val) {
                    $question[$qvalue->qtid][] = $val->questionid;
                    $qcount++;
                    $qids[] = $val->questionid;
                }
            }
        }
        $user = $this->session->get("user");
        $course = MasterCourse::findFirst(array("id='" . $user['course'] . "'"));
        $subjectid = json_decode($course->subject);
        foreach ($subjectid as $idall) {
            $course_sub = MasterSubject::findFirst(array("id='" . $idall . "'"));
            $subject[$idall] = ucfirst($course_sub->subjectname);
        }
        $this->view->setVar("course", $course);
        $this->view->setVar("subject", $subject);
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("qtype", $mqtype);
        $this->view->setVar("questionsIDlist", $question);
        $this->view->setVar("freetest", $freetest);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
        $this->view->setVar("uids", $id);
    }

    function fullsyllabusAction($id_test='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($id_test == '') {
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='5'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("id='" . $id_test . "'"));
        $exam_id = MasterExamType::findFirst(array("etid='" . $master_exam->exam_type . "'"));
        $this->view->setVar("master_exam", $master_exam);
        $subjects = json_decode($master_exam->sort);
        $q_type = json_decode($master_exam->qint);
        $subject = MasterSubject::find("status='1'");
        $qcount = 0;
        $qcount = 0;
        foreach ($q_type as $key => $va) {
            $topicwise = MasterTestExam::find(array("id_test=$id_test"));
            foreach ($topicwise as $valtopic) {
                $used = json_decode($valtopic->qint);
                $totn[$key][] = $used->$key;
                foreach ($totn[$key] as $vanew) {
                    $easy = $vanew[1];
                    $medium = $vanew[2];
                    $hard = $vanew[3];
                }
                $level[1] = $easy ? $easy : 0;
                $level[2] = $medium ? $medium : 0;
                $level[3] = $hard ? $hard : 0;
                for ($i = 1; $i <= 3; $i++) {
                    $qus = MasterQuestion::find(array("topics='" . $valtopic->topic . "' AND q_type='" . $key . "' AND q_level='{$i}' AND e_type LIKE '%2%' ORDER BY rand() LIMIT " . $level[$i] . "  "));
                    if (!empty($qus)) {
                        foreach ($qus as $val) {
                            $questions[$key][] = $val->questionid;
                            $qcount++;
                            $qids[] = $val->questionid;
                            $qidsub[$val->questionid] = $val->subject;
                        }
                    }
                }
            }
        }
        $this->view->setVar("questionsIDlist", $questions);
        $this->view->setVar("etype", $exam_id);
        $this->view->setVar("subject", $subject);
        $this->view->setVar("selectsubject", array_flip($subjects));
        $mqtype = MasterQuesType::find("status=1");
        $this->view->setVar("mqtype", $mqtype);
        $this->view->setVar("numqus", $qcount);
        $this->view->setVar("qids", $qids);
        $this->view->setVar("qidsub", $qidsub);
    }

    function swtdescAction($test_id='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($test_id == '') {
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='1'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $master_exam = MasterTests::findFirst(array("id='" . $test_id . "'"));
        $this->view->setVar("master_exam", $master_exam);
    }

    function uwtdescAction($id) {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='3'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $master_exam = MasterTests::findFirst("id='" . $id . "'");
        $this->view->setVar("master_exam", $master_exam);
    }

    function fsdescAction($id_test='') {

        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($id_test == '') {
            return $response->redirect("index/dashboard");
        }

        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='5'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }

        $questions = array();
        $master_exam = MasterTests::findFirst(array("id='" . $id_test . "'"));
        $this->view->setVar("master_exam", $master_exam);
    }

    function twtdescAction($exam_type='', $topic='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        if ($exam_type == '' || $topic == '') {
            return $response->redirect("index/dashboard");
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='2'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $sup_cat = $user_session['sup_cat'];
        $exam_id = MasterExamType::findFirst(array("exam_type='" . $exam_type . "' AND sup_cat='" . $sup_cat . "'"));
        $couse = $user_session['course'];
        $master_exam = MasterTests::findFirst(array("test_type='" . $exam_type . "' AND exam_type='" . $exam_id->etid . "' AND couse='" . $user_session['course'] . "' AND status='1'"));
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("topic", $topic);
        $this->view->setVar("submit_redirect", $exam_type . '/' . $topic);
    }

    function fwtdescAction($id) {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterUserPackage::findFirst("userid='" . $user_session['id'] . "' AND test_type='3'");
        if (empty($package)) {
            return $response->redirect("index/dashboard");
        }
        $questions = array();
        $master_exam = MasterTests::findFirst("id='" . $id . "'");
        $this->view->setVar("master_exam", $master_exam);
        $this->view->setVar("submit_redirect", $id);
    }

    function fixpackAction($packid='') {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        } else {
            return $response->redirect("index");
        }
        $upack = MasterUserPackage::find("userid='{$user_session['id']}' AND test_type='4'");
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $package = MasterPackages::find("course='{$user_session['course']}' AND test_type='4' AND status=1");
        $i = 0;
        foreach ($package as $vz) {
            $arr[$i] = json_decode($vz->fixtest_id);
            $arr[$i]['id_package'] = $vz->id;
            $i++;
        }
        $j=0;
        foreach ($arr as $key => $value) {
            foreach ($value as $ke => $valu) {
                if(!empty($valu[0]) && !empty($valu[1])){
                    $time=strtotime($valu[1]);
                $alllist[$j]['frtime']=$valu[2];
                 $alllist[$j]['date']=$valu[1];
                $alllist[$j]['id']=$valu[0];
                $alllist[$j]['totime']=$valu[3];
                $alllist[$j]['id_package']=$value['id_package'];
                $j++;
            }
        }
    }
    foreach ($alllist as $key => $part) {
       $sort[$key] = strtotime($part['date']);
    }
   array_multisort($sort, SORT_ASC,$alllist);
   $timegrt=strtotime("-1 year 6 Month");
   $k=0;
   $da=array();
   foreach($alllist as $data){
    $year=date("Y",strtotime($data['date']));
    $month=strtoupper(date("M",strtotime($data['date'])));
    if(strtotime($data['date'])>=$timegrt){
        $da[$year][$month][$k]['date']=$data['date'];
        $da[$year][$month][$k]['frtime']=$data['frtime'];
        $da[$year][$month][$k]['id']=$data['id'];
        $da[$year][$month][$k]['totime']=$data['totime'];
        $da[$year][$month][$k]['id_package']=$data['id_package'];
        $da[$year][$month][$k]['fromtimestamp']=strtotime($data['date']." ".$data['frtime']);
        $da[$year][$month][$k]['totimestamp']=strtotime($data['date']." ".$data['totime']);
        $k++;
    }
   }
        $fixtest = MasterFixtest::find("sup_cat='{$user_session['sup_cat']}' AND course='{$user_session['course']}'");
        foreach ($fixtest as $key => $value) {
            $ft[$value->id] = $value->name;
        }
        $userpack=array();
        foreach($upack as $res){
            $userpack[]=$res->packageid;
        }
        $this->view->setVar("fixtest", $ft);
        $this->view->setVar("package", $package);
        $this->view->setVar("course", $course);
        $this->view->setVar("upack", $userpack);
        $this->view->setVar("fixpack", $da);
    }
    function checkpointsAction($id) {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $package = MasterPackages::findFirst("id='{$id}'");
        $point = MasterUserpoint::findFirst("uid='{$user_session['id']}'");
        if ($package->price < $point->point || $package->price == $point->point) {
            
        }
    }

    function quizAction($id) {
        $response = new \Phalcon\Http\Response();
        $postval = $this->request->getPost();
        if ($this->session->has("user")) {
            $user_session = $this->session->get("user");
            $this->view->setVar("user_session", $user_session);
        }
        $but = MasterQuiz::findFirst("quiz_id='{$id}'");
    }

    function updatetestsAction($id='') {
        $response = new \Phalcon\Http\Response();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $postval['sort'] = json_encode($postval['sort']);
            $extyid = $postval['exam_type'];
            $postval['qint'] = json_encode($postval['qint']);
            $Addques = new MasterTests();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Test Successfully Updated</div>");
            return $response->redirect("exams");
        }
        if ($id != '') {
            $postval = $this->request->getPost();
            $exam_data = MasterTests::findFirst(array("id='" . $id . "'"));
            $this->view->setVar("exam_data", $exam_data);
            $test_type = $exam_data->test_type;
            $exam_type = MasterExamType::find(array("exam_type='" . $test_type . "' AND status='1'"));
            $this->view->setVar("exam_type", $exam_type);
            $exam_type = MasterExamType::findFirst(array("etid='" . $exam_data->exam_type . "'"));
            $examcourse = json_decode($exam_type->course);
            foreach ($examcourse as $val) {
                $course = MasterCourse::findFirst(array("id='" . $val . "' AND status=1"));
                $coursenew[$course->id] = $course->name;
            }
            $this->view->setVar("course", $coursenew);
            $supcat = MasterSupCat::find("status=1");
            $supca = array();
            foreach ($supcat as $v) {
                $supca[$v->scid] = $v->scname;
            }
            $this->view->setVar("supcat", $supca);
            $course = MasterCourse::find("status=1");
            $couca = array();
            foreach ($course as $v) {
                $couca[$v->id] = $v->name;
            }
            $this->view->setVar("coursenew", $couca);
        }
    }

    public function getalltopicAction() {
        $getVal = $this->request->getPost();
        $subject = $getVal['subject'];
        $subsubject = MasterSubSubject::find(array("subid='$subject' AND status=1"));
        foreach ($subsubject as $ssres) {
            $topics = MasterTopics::find(array("subid='" . $subject . "' AND ssubid='" . $ssres->ssid . "' AND status=1"));
            if ($topics->count()) {
                echo '<div class="col-md-4"><div class="choose-topics-content"><p><b>' . $ssres->ssname . '</b></p>';
                foreach ($topics as $value) {
                    ?>
                    <div class="col-md-12">
                        <div class="checkbox-custom checkbox-default">
                            <input type="checkbox" id="curs<?= $value->tid ?>" value="<?= $value->tid ?>" class="chkcnt" name="sort[]">
                            <label for="curs<?= $value->tid ?>" ><?= $value->tname ?></label>
                        </div>
                    </div>
                    <?php
                }
                echo "</div></div>";
            }
        }
    }

    public function managetopicsAction($testtype='', $id_test='', $course_new='', $etid='') {
        $response = new \Phalcon\Http\Response();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $supcat = MasterSupCat::find("status=1");
        $supca = array();
        foreach ($supcat as $v) {
            $supca[$v->scid] = $v->scname;
        }
        $this->view->setVar("supcat", $supca);
        $course = MasterCourse::find("status=1");
        $couca = array();
        foreach ($course as $v) {
            $couca[$v->id] = $v->name;
        }
        $this->view->setVar("course", $couca);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("ques_type", $qtype);
        $this->view->setVar("test_id", $id_test);
        $this->view->setVar("test_type", $testtype);
        $this->view->setVar("exam_type", $etid);
        $this->view->setVar("couse", $course_new);
    }

    public function manageunitwiseAction($testtype='', $id_test='', $course_new='', $etid='') {
        $response = new \Phalcon\Http\Response();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $supcat = MasterSupCat::find("status=1");
        $supca = array();
        foreach ($supcat as $v) {
            $supca[$v->scid] = $v->scname;
        }
        $this->view->setVar("supcat", $supca);
        $course = MasterCourse::find("status=1");
        $couca = array();
        foreach ($course as $v) {
            $couca[$v->id] = $v->name;
        }
        $this->view->setVar("course", $couca);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("ques_type", $qtype);
        $this->view->setVar("test_id", $id_test);
        $this->view->setVar("test_type", $testtype);
        $this->view->setVar("exam_type", $etid);
        $this->view->setVar("couse", $course_new);
    }

    public function managetopicwiseAction($testtype='', $id_test='', $course_new='', $etid='') {
        $response = new \Phalcon\Http\Response();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $supcat = MasterSupCat::find("status=1");
        $supca = array();
        foreach ($supcat as $v) {
            $supca[$v->scid] = $v->scname;
        }
        $this->view->setVar("supcat", $supca);
        $course = MasterCourse::find("status=1");
        $couca = array();
        foreach ($course as $v) {
            $couca[$v->id] = $v->name;
        }
        $this->view->setVar("course", $couca);
        $qtype = MasterQuesType::find("status=1");
        $this->view->setVar("ques_type", $qtype);
        $this->view->setVar("test_id", $id_test);
        $this->view->setVar("test_type", $testtype);
        $this->view->setVar("exam_type", $etid);
        $this->view->setVar("couse", $course_new);
    }

    public function gettopicwiseAction() {
        $getVal = $this->request->getPost();
        $course = $getVal['course'];
        $test_type = $getVal['test_type'];
        $topic = $getVal['topic'];
        $etid = $getVal['etid'];
        $test_id = $getVal['test_id'];
        $subjects = MasterSubject::find(array("status='1'"));
        $q_type = MasterExamType::findFirst(array("etid='" . $etid . "'"));
        $neq_qtype = $q_type;
        $ques_type = MasterQuesType::find("status=1");
        $q_type = json_decode($q_type->q_type);
        $sql = "SELECT q_type,count(*) as total FROM MasterQuestion WHERE topics={$topic} AND LOCATE(2,e_type) GROUP BY q_type";
        $sql_level = "SELECT q_type,q_level,count(*) as total FROM MasterQuestion WHERE topics={$topic} AND LOCATE(2,e_type) GROUP BY q_type,q_level";

        $res = $this->modelsManager->executeQuery($sql);
        $arr = array();
        foreach ($res as $val) {
            $arr['total'][$val->q_type] = $val->total;
        }
        $arr_lev = array();
        $res_level = $this->modelsManager->executeQuery($sql_level);
        foreach ($res_level as $val) {
            $arr_lev['type'][$val->q_type][$val->q_level] = $val->total;
        }
        $topicdata = MasterTestExam::findfirst(array("id_test=$test_id AND topic=$topic"));
        if (!empty($topicdata)) {
            $newdata = json_decode($topicdata->qint);
        }
        foreach ($newdata as $key => $da) {
            foreach ($da as $k => $d) {
                $alldata[$key][$k] = $d;
            }
        }
        $remain = MasterTestExam::find(array("id_test=$test_id"));
        $totalused = 0;
        foreach ($remain as $va) {
            $used = json_decode($va->qint);
            foreach ($used as $key => $val) {
                $totalused+= $val[0];
            }
        }
        $remain_ques = (int) $neq_qtype->no_qs - (int) $totalused;
        ?>
        <input type="hidden" name="topic"  value="<?php echo $topic; ?>">
        <?php if (!empty($topicdata)) { ?>
            <input type="hidden" name="id" value="<?php echo $topicdata->id; ?>">
        <?php } ?>
        <div class="col-md-2 pull-right">
            <button type="button" class="btn btn-primary btn-md btn-block" onclick="validate();">Save</button>
            <br>
        </div>
        <?php
        $i = 0;
        $j = 1;
        foreach ($ques_type as $value) {
            foreach ($q_type as $val) {
                if ($value->qtid == $val) {
                    ?>
                    <div class="qs-type-con">
                        <input type="hidden" name="qint<?= $val ?>[]" value="<?= $val ?>" class="form-control">
                        <div class="qs-type-list">
                            <h5 class="qs-type-heading"><?= $value->qtname ?><span id="qt<?= $value->qtid ?>">(<b><?php echo $arr['total'][$val] ? $arr['total'][$val] : 0; ?></b>)</span></h5>
                            <div class="col-md-3">
                                <label for="">No. Q</label>
                                <input type="text" name="qint[<?= $val ?>][]" value="<?php echo $alldata[$val][0]; ?>" class="form-control total" id="total<?php echo $val; ?>" onblur="totalques(<?php echo $val; ?>,this.value);">
                            </div>
                            <div class="col-md-3">
                                <label for="">Easy<span id="tt<?= $value->qtid ?>1">(<b><?php echo $arr_lev['type'][$val][1] ? $arr_lev['type'][$val][1] : 0; ?></b>)</span></label>
                                <input type="text" name="qint[<?= $val ?>][]" value="<?php echo $alldata[$val][1]; ?>" class="form-control que<?= $val ?>" id="que<?= $val ?>1" onblur="totaltype(<?php echo $val; ?>,this.value,1);">
                            </div>
                            <div class="col-md-3">
                                <label for="">Medium<span id="tt<?= $value->qtid ?>2">(<b><?php echo $arr_lev['type'][$val][2] ? $arr_lev['type'][$val][2] : 0; ?></b>)</span></label>
                                <input type="text" name="qint[<?= $val ?>][]" value="<?php echo $alldata[$val][2]; ?>" class="form-control que<?= $val ?>" id="que<?= $val ?>2" onblur="totaltype(<?php echo $val; ?>,this.value,2);">
                            </div>
                            <div class="col-md-3">
                                <label for="">Hard<span id="tt<?= $value->qtid ?>3">(<b><?php echo $arr_lev['type'][$val][3] ? $arr_lev['type'][$val][3] : 0; ?></b>)</span></label>
                                <input type="text" name="qint[<?= $val ?>][]" value="<?php echo $alldata[$val][3]; ?>" class="form-control que<?= $val ?>" id="que<?= $val ?>3" onblur="totaltype(<?php echo $val; ?>,this.value,3);">
                            </div>
                        </div>
                    </div>                                                            <div class="clearfix"></div>
                    <?php
                    $i = $i + 5;
                    $j++;
                }
            }
        }
        ?>

        <?php
    }

    public function addmanagetopicsAction() {
        if (!$this->session->has("admin")) {
            header("location:" . BASEURL . 'admin/login');
        }
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval = $this->request->getPost();
            $extyid = $postval['exam_type'];
            $qint = array();
            $exam = MasterExamType::findFirst("etid='" . $extyid . "' AND status=1");
            $postval['qint'] = json_encode($postval['qint']);
            $postval['status'] = 1;
            $Addques = new MasterTestExam();
            $Addques->save($postval);
            $this->flashSession->success(" <div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Topic Managed Successfully</div>");
            return $response->redirect($postval['url']);
        }
    }

    public function subjecttypeAction($type='', $course='', $subnew='') {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name', $course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
        $subject_new = MasterSubject::findFirst(array("slug='" . $subnew . "'"));
        $sql = "SELECT * FROM MasterTests  WHERE test_type=1 AND couse={$user_session['course']} AND sort LIKE '%" . $subject_new->id . "%'";
        $res_toal = $this->modelsManager->executeQuery($sql);
        $prac = array();
        $i = 0;
        foreach ($res_toal as $va) {
            $examtype = MasterExamType::findFirst(array("etid='" . $va->exam_type . "' AND status=1"));
            $remain = MasterTestExam::find(array("id_test=$va->id"));
            $totalused = 0;
            if (!empty($remain)) {
                foreach ($remain as $remva) {
                    $used = json_decode($remva->qint);
                    foreach ($used as $key => $valnew) {
                        $totalused+= $valnew[0];
                    }
                }
            }
            if (!empty($examtype) && ($examtype->no_qs == $totalused)) {
                $prac[$i]['id'] = $va->id;
                $prac[$i]['etname'] = $examtype->etname;
                $prac[$i]['no_qs'] = $examtype->no_qs;
                $prac[$i]['time'] = $examtype->time;
                $i++;
            }
        }
        $this->view->setVar("alllist", $prac);
    }

    public function unittypsAction($type='', $course_new='', $subnew='') {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $course_new . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name', $course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
        $subject_new = MasterSubject::findFirst(array("slug='" . $subnew . "'"));
        $alltopics = MasterTopics::find(array("subid='" . $subject_new->id . "'"));
        $i = 0;
        $prac = array();
        foreach ($alltopics as $va) {
            $sql_qt = "SELECT * FROM MasterTests WHERE test_type=3 AND couse=$course_new AND sort LIKE '%" . $va->tid . "%'";
            $res_toal = $this->modelsManager->executeQuery($sql_qt);
            $j = 0;
            foreach ($res_toal as $vanew) {
                $prac[$i][$j] = $vanew->id;
                $j++;
            }
            $i++;
        }
        $new = array();
        foreach ($prac as $va) {
            foreach ($va as $v) {
                $new[] = $v;
            }
        }
        $total_id = implode(",", array_unique($new));
        $finprac = array();
        $i = 0;
        if (!empty($total_id)) {
            $sql_qtt = "SELECT * FROM MasterTests WHERE id IN({$total_id})";
            $sql_qtt = $this->modelsManager->executeQuery($sql_qtt);
            foreach ($sql_qtt as $va) {
                $examtype = MasterExamType::findFirst(array("etid='" . $va->exam_type . "' AND status=1"));
                $remain = MasterTestExam::find(array("id_test=$va->id"));
                $totalused = 0;
                if (!empty($remain)) {
                    foreach ($remain as $remva) {
                        $used = json_decode($remva->qint);
                        foreach ($used as $key => $valnew) {
                            $totalused+= $valnew[0];
                        }
                    }
                }

                if (!empty($examtype) && ($examtype->no_qs == $totalused)) {
                    $finprac[$i]['id'] = $va->id;
                    $finprac[$i]['etname'] = $examtype->etname;
                    $finprac[$i]['no_qs'] = $examtype->no_qs;
                    $finprac[$i]['time'] = $examtype->time;
                    $finprac[$i]['name'] = $va->unit;
                    $i++;
                }
            }
        }
        $this->view->setVar("alllist", $finprac);
    }

    public function topictypeAction($type='', $topic='') {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name', $course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);

        $sql = "SELECT * FROM MasterTests  WHERE test_type=2 AND couse={$user_session['course']} AND sort LIKE '%" . $topic . "%'";
        $res_toal = $this->modelsManager->executeQuery($sql);
        $prac = array();
        $i = 0;
        foreach ($res_toal as $va) {
            $examtype = MasterExamType::findFirst(array("etid='" . $va->exam_type . "' AND status=1"));
            $remain = MasterTestExam::find(array("id_test=$va->id AND topic=$topic"));
            $totalused = 0;
            if (!empty($remain)) {
                foreach ($remain as $remva) {
                    $used = json_decode($remva->qint);
                    foreach ($used as $key => $valnew) {
                        $totalused+= $valnew[0];
                    }
                }
            }
            if (!empty($examtype) && ($examtype->no_qs == $totalused)) {
                $prac[$i]['id'] = $va->id;
                $prac[$i]['etname'] = $examtype->etname;
                $prac[$i]['no_qs'] = $examtype->no_qs;
                $prac[$i]['time'] = $examtype->time;
                $prac[$i]['topic'] = $topic;
                $i++;
            }
        }
        $this->view->setVar("alllist", $prac);
    }

    public function syllabustypeAction() {
        $response = new \Phalcon\Http\Response();
        if (!$this->session->has("user")) {
            return $response->redirect("index");
        }
        $user_session = $this->session->get("user");
        $this->view->setVar("user_session", $user_session);
        $course = MasterCourse::findFirst(array("id='" . $user_session['course'] . "'"));
        $this->view->setVar("course", $course);
        $this->session->set('course_name', $course->name);
        $subject = MasterSubject::find(array("status=1"));
        $su = "";
        foreach ($subject as $sub) {
            $su[$sub->id]['name'] = $sub->subjectname;
            $su[$sub->id]['id'] = $sub->id;
            $su[$sub->id]['slug'] = $sub->slug;
        }
        $this->view->setVar("subject", $su);
        $sql = "SELECT * FROM MasterTests  WHERE test_type=5 AND couse={$user_session['course']}";
        $res_toal = $this->modelsManager->executeQuery($sql);
        $prac = array();
        $i = 0;
        foreach ($res_toal as $va) {
            $examtype = MasterExamType::findFirst(array("etid='" . $va->exam_type . "' AND status=1"));
            $remain = MasterTestExam::find(array("id_test=$va->id"));
            $totalused = 0;
            if (!empty($remain)) {
                foreach ($remain as $remva) {
                    $used = json_decode($remva->qint);
                    foreach ($used as $key => $valnew) {
                        $totalused+= $valnew[0];
                    }
                }
            }
            if (!empty($examtype) && ($examtype->no_qs == $totalused)) {
                $prac[$i]['id'] = $va->id;
                $prac[$i]['etname'] = $examtype->etname;
                $prac[$i]['no_qs'] = $examtype->no_qs;
                $prac[$i]['time'] = $examtype->time;
                $i++;
            }
        }
        $this->view->setVar("alllist", $prac);
    }
    public function getdescriptionAction(){
        $response = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $postval=$this->request->getPost();
            $fixtest = MasterFixtest::findFirst("id='".$postval['id_exam']."'"); ?>
            <div class="modal-body">
            <p id="content"><?=$fixtest->description?></p>
          </div>
          <div class="modal-footer" <?php if(time() < (int)$postval['fromtime']){?> style="display:none;" <?php } ?> >
            <a href="<?=BASEURL?>tests/fixedtest/<?=$postval['id_exam']?>" class="btn btn-primary">Proceed..</a>
          </div>
       <?php }
    }

}
?>