<?php

class AdminController extends ControllerBase {

    public function initialize() {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Ischool4u | ADMIN');
        parent::initialize();
        $access = $this->session->get('admin');
        $this->view->setVar("access_roles", $access);
    }

    public function indexAction() {
        $response = new \Phalcon\Http\Response();
        $tables = array('1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion','6'=>'MasterSubtheoQuestion');
        if (!$this->session->has("admin")) {
            return $response->redirect("admin/login");
        }
        $dnote = MasterDnote::find(array("status=1 AND type='2'", "order" => "id DESC", "group" => "questionid", "limit" => 7));
        $darray=array();
        $i=0;
        foreach($dnote as $value){
            $darray[$i]['questionid']=$value->questionid;
            $darray[$i]['note']=$value->note;
            $darray[$i]['id']=$value->id;
            $enotedat = MasterQuestion::findFirst("questionid='{$value->questionid}'");
            $qdetail = $tables[$enotedat->tableid]::findFirst("questionid='{$value->questionid}'");
            $darray[$i]['question']=$qdetail->question;
            $darray[$i]['qusid']=$enotedat->qusid;
            $i++;
        }
        $this->view->setVar("dnote", $darray);
        $enote = MasterDnote::find(array("status=1 AND type='1'", "order" => "id DESC", "group" => "questionid", "limit" => 7));
                $j=0;
        $earray=array();
        foreach($enote as $value){
            $earray[$j]['questionid']=$value->questionid;
            $earray[$j]['note']=$value->note;
            $earray[$j]['id']=$value->id;
            $enotedat = MasterQuestion::findFirst("questionid='{$value->questionid}'");
            $qdetail = $tables[$enotedat->tableid]::findFirst("questionid='{$value->questionid}'");
            $earray[$j]['question']=$qdetail->question;
            $earray[$j]['qusid']=$enotedat->qusid;
            $j++;
        }
        $this->view->setVar("enote", $earray);
    }

    public function loginAction() {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("admin")) {
            return $response->redirect("admin");
        }
        if ($this->request->isPost()) {
            //echo"<pre>";print_r($this->request->getPost());exit();
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            //
            $admin = MasterAdminUsers::findFirst(array("mail_id = :email: AND pass=:password: AND is_active=:is_active:", 'bind' => array('email' => $email, "password" => base64_encode($password), "is_active" => 1)));

            if ($admin) {
                $id = $admin->aid;
                $name = $admin->first_name . " " . $admin->last_name;
                $type = ($admin->type == '3') ? 'superadmin' : 'admin';
                $role = MasterRole::findFirst("id='" . $admin->type . "' ");
                $this->session->set('admin', array(
                    'id' => $id,
                    'type' => $type,
                    'name' => $name,
                    'role_name' => $role->role_name,
                    'access_role' => $role->role_access,
                    'role' => $role->role,
                    'is_verifier' => $role->is_verifier,
                ));
                return $response->redirect("admin");
            } else {
                $this->flashSession->success(" <div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Invalid Login Credential. Please try again with valid login id and password !</div>");
                return $response->redirect("admin/login");
            }
        }
    }

    public function endAction() {
        $response = new \Phalcon\Http\Response();
        $this->session->remove('admin');
        $this->flash->success('Goodbye!');
        return $response->redirect("admin/login");
    }

    public function signupAction() {
        
    }

    function sdoubtAction($id) {
        $response = new \Phalcon\Http\Response();
        if ($this->session->has("admin")) {
            $user_session = $this->session->get("admin");
        } else {
            return $response->redirect("admin");
        }
            $postVal = $this->request->getPost();
            UIElementsAdmin::callfromupdate($id,$postVal['solved'],$postVal['type'],$postVal['questionid'],$user_session['id']);
                if($postVal['type']==1){
                    return $response->redirect("admin/index");
                }else {
                    return $response->redirect("admin/index");
                }
    }

    function viewnoteAction() {
       $tables = array('1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion','6'=>'MasterSubtheoQuestion');
        $response = new \Phalcon\Http\Response();
        if(!$this->session->has("admin"))
        {  
            return $response->redirect("admin/login");
        }
        $dnote = $this->modelsManager->executeQuery("SELECT * FROM MasterDnote WHERE status=1 AND type='2' ORDER BY id DESC");
        $darray=array();
        $i=0;
        foreach($dnote as $value){
            $darray[$i]['questionid']=$value->questionid;
            $darray[$i]['note']=$value->note;
            $darray[$i]['id']=$value->id;
            $enotedat = MasterQuestion::findFirst("questionid='{$value->questionid}'");
            $qdetail = $tables[$enotedat->tableid]::findFirst("questionid='{$value->questionid}'");
            $darray[$i]['question']=$qdetail->question;
            $darray[$i]['qusid']=$enotedat->qusid;
            $i++;
        }
        $this->view->setVar("dnote",$darray);
    }

    function viewerrorAction() {
        $tables = array('1'=>'MasterQuestionBank','2'=>'MasterMatchQuestion','3'=>'MasterNumericQuestion','4'=>'MasterParaQuestion','5'=>'MasterReasonQuestion','6'=>'MasterSubtheoQuestion');
        $response = new \Phalcon\Http\Response();
        if(!$this->session->has("admin"))
        {  
            return $response->redirect("admin/login");
        }
        $enote = $this->modelsManager->executeQuery("SELECT * FROM MasterDnote WHERE status=1 AND type='1' ORDER BY id DESC");
        $j=0;
        $earray=array();
        foreach($enote as $value){
            $earray[$j]['questionid']=$value->questionid;
            $earray[$j]['note']=$value->note;
            $earray[$j]['id']=$value->id;
            $enotedat = MasterQuestion::findFirst("questionid='{$value->questionid}'");
            $qdetail = $tables[$enotedat->tableid]::findFirst("questionid='{$value->questionid}'");
            $earray[$j]['question']=$qdetail->question;
            $earray[$j]['qusid']=$enotedat->qusid;
            $j++;
        }
        $this->view->setVar("enote",$earray);
    }

}