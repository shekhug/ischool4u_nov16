function adduser_validate(){
    //alert(123);
    var validator = $("#adduser").validate({
        rules: {
            first_name:"required",
            last_name: "required",
            mail_id:"required",
            pass: "required",
            contact_no:"required",
            sup_cat: "required",
            course:"required",
            mem_start: "required",
            address:"required",
            city: "required",
            state:"required",
            zip:"required",
            country:"required",
            marketing_pref:"required",
            is_active:"required"

        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var y = validator.form();
    if(y){
        return true;
    }else{
        return false;
    }
 }
 function addnews_validate(){
    alert(123);
    var validator = $("#addnews").validate({
        rules: {
            title_news:"required",
            slug: "required",
            description:"required",
            status: "required"

        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var y = validator.form();
    if(y){
        return true;
    }else{
        return false;
    }
 }