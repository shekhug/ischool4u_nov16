
/**
 * setEditor is an javascript funtion to collect description data than send it to Editor.
 * @param {string} para [class of textarea]
 */
function setEditor(para)
{
	var param = '.'+para;
	var value = $(param).val();
	$('.Editor-editor').attr('id','Editor-editor');
	document.getElementById("Editor-editor").innerHTML = value;
	document.getElementById("btn-description").style.display = "none";	
	if (document.getElementById("btn-ma") != null){
		document.getElementById("btn-ma").style.display = "none";
		document.getElementById("btn-mb").style.display = "none";
		document.getElementById("btn-mc").style.display = "none";
		document.getElementById("btn-md").style.display = "none";
		document.getElementById("btn-me").style.display = "none";
		document.getElementById("btn-mp").style.display = "none";
		document.getElementById("btn-mq").style.display = "none";
		document.getElementById("btn-mr").style.display = "none";
		document.getElementById("btn-ms").style.display = "none";
		document.getElementById("btn-mt").style.display = "none";
	}
	
	if(para=='description'){
		document.getElementById("btn-description").style.display = "inline";
	
	}
}
function setInputArea(para)
{
	var value = document.getElementById(para).value;
	$('.Editor-editor').attr('id','Editor-editor');
	document.getElementById("Editor-editor").innerHTML = value;
	document.getElementById("Editor-editor").innerHTML = value;
	document.getElementById("btn-description").style.display = "none";	
	document.getElementById("btn-ma").style.display = "none";
	document.getElementById("btn-mb").style.display = "none";
	document.getElementById("btn-mc").style.display = "none";
	document.getElementById("btn-md").style.display = "none";
	document.getElementById("btn-me").style.display = "none";
	document.getElementById("btn-mp").style.display = "none";
	document.getElementById("btn-mq").style.display = "none";
	document.getElementById("btn-mr").style.display = "none";
	document.getElementById("btn-ms").style.display = "none";
	document.getElementById("btn-mt").style.display = "none";
	if(para=='ma'){
		document.getElementById("btn-ma").style.display = "inline";
	}else if(para=='mb'){
		document.getElementById("btn-mb").style.display = "inline";
	}else if(para=='mc'){
		document.getElementById("btn-mc").style.display = "inline";
	}else if(para=='md'){
		document.getElementById("btn-md").style.display = "inline";
	}else if(para=='me'){
		document.getElementById("btn-me").style.display = "inline";
	}else if(para=='mp'){
		document.getElementById("btn-mp").style.display = "inline";
	}else if(para=='mq'){
		document.getElementById("btn-mq").style.display = "inline";
	}else if(para=='mr'){
		document.getElementById("btn-mr").style.display = "inline";
	}else if(para=='ms'){
		document.getElementById("btn-ms").style.display = "inline";
	}else if(para=='mt'){
		document.getElementById("btn-mt").style.display = "inline";
	}
}
/**
 * setTextArea is an javascript funtion to collect data from text area to there respective field.
 * @param {string} para [id of textarea]
 */
function setTextArea(para)
{
	var param = '#'+para;
	var value = $('#Editor-editor').html();

	if(para=='description'){
		document.getElementById("description").value = value;
	}else if(para=='ma'){
		document.getElementById("ma").value = value;
		document.getElementById("ma").removeAttribute("readonly");
	}else if(para=='mb'){
		document.getElementById("mb").value = value;
		document.getElementById("mb").removeAttribute("readonly");
	}else if(para=='mc'){
		document.getElementById("mc").value = value;
		document.getElementById("mc").removeAttribute("readonly");
	}else if(para=='md'){
		document.getElementById("md").value = value;
		document.getElementById("md").removeAttribute("readonly");
	}else if(para=='me'){
		document.getElementById("me").value = value;
		document.getElementById("me").removeAttribute("readonly");
	}else if(para=='mp'){
		document.getElementById("mp").value = value;
		document.getElementById("mp").removeAttribute("readonly");
	}else if(para=='mq'){
		document.getElementById("mq").value = value;
		document.getElementById("mq").removeAttribute("readonly");
	}else if(para=='mr'){
		document.getElementById("mr").value = value;
		document.getElementById("mr").removeAttribute("readonly");
	}else if(para=='ms'){
		document.getElementById("ms").value = value;
		document.getElementById("ms").removeAttribute("readonly");
	}else if(para=='mt'){
		document.getElementById("mt").value = value;
		document.getElementById("mt").removeAttribute("readonly");
	}
}

function getChecked(para,id,ans)
{
	var param = '#'+para;
	var checked = document.getElementById(para).checked;
	//alert(checked);
	var value = document.getElementById(para).value;
	if(checked == true){
		document.getElementById(id).value += value;
		document.getElementById(ans).value += value;
	}else{
		var str = document.getElementById(id).value;
		var value = str.replace(value, '');
		document.getElementById(id).value = value;
		document.getElementById(ans).value = value;
	}
}

