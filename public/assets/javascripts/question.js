
/**
 * setEditor is an javascript funtion to collect question and option click and data than send it to Editor.
 * @param {string} para [class of textarea]
 */
function setEditor(para)
{
	var param = '.'+para;
	var value = $(param).val();
	$('.Editor-editor').attr('id','Editor-editor');
         CKEDITOR.instances['ques_name'].setData(value);
	//document.getElementById("Editor-editor").innerHTML = value;
	document.getElementById("btn-question").style.display = "none";
	document.getElementById("btn-option1").style.display = "none";
	document.getElementById("btn-option2").style.display = "none";
	document.getElementById("btn-option3").style.display = "none";
	document.getElementById("btn-option4").style.display = "none";
	document.getElementById("btn-hint").style.display = "none";
	if (document.getElementById("btn-ma") != null){
		document.getElementById("btn-ma").style.display = "none";
		document.getElementById("btn-mb").style.display = "none";
		document.getElementById("btn-mc").style.display = "none";
		document.getElementById("btn-md").style.display = "none";
		document.getElementById("btn-me").style.display = "none";
		document.getElementById("btn-mp").style.display = "none";
		document.getElementById("btn-mq").style.display = "none";
		document.getElementById("btn-mr").style.display = "none";
		document.getElementById("btn-ms").style.display = "none";
		document.getElementById("btn-mt").style.display = "none";
	}
	
	if(para=='question'){
		document.getElementById("btn-question").style.display = "inline";
	}else if(para=='option1'){
		document.getElementById("btn-option1").style.display = "inline";
	}else if(para=='option2'){
		document.getElementById("btn-option2").style.display = "inline";
	}else if(para=='option3'){
		document.getElementById("btn-option3").style.display = "inline";
	}else if(para=='option4'){
		document.getElementById("btn-option4").style.display = "inline";
	}else if(para=='hint'){
		document.getElementById("btn-hint").style.display = "inline";
	}
}

function setnuEditor(para)
{
	var param = '.'+para;
	var value = $(param).val();
	$('.Editor-editor').attr('id','Editor-editor');
         CKEDITOR.instances['ques_name'].setData(value);
	document.getElementById("btn-question").style.display = "none";
	document.getElementById("btn-option1").style.display = "none";
	document.getElementById("btn-option2").style.display = "none";
	document.getElementById("btn-hint").style.display = "none";
	
	if(para=='question'){
		document.getElementById("btn-question").style.display = "inline";
	}else if(para=='option1'){
		document.getElementById("btn-option1").style.display = "inline";
	}else if(para=='option2'){
		document.getElementById("btn-option2").style.display = "inline";
	}else if(para=='hint'){
		document.getElementById("btn-hint").style.display = "inline";
	}
}

function setparaEditor(para)
{
	var value = $('.'+para).val();
	$('.Editor-editor').attr('id','Editor-editor');
         CKEDITOR.instances['ques_name'].setData(value);
    var j=1;
    for(var i=1;i<=4;i++){
    	document.getElementById("btn-question"+i).style.display = "none";
		document.getElementById("btn-option"+(j++)).style.display = "none";
		document.getElementById("btn-option"+(j++)).style.display = "none";
		document.getElementById("btn-option"+(j++)).style.display = "none";
		document.getElementById("btn-option"+(j++)).style.display = "none";
    }
	document.getElementById("btn-hint").style.display = "none";
	for(i=1;i<=4;i++){
		if(para=='question'+i){
			document.getElementById("btn-question"+i).style.display = "inline";
		}
	}
	for(j=1;j<=16;j++){
		if(para=='option'+j){
			document.getElementById("btn-option"+j).style.display = "inline";
		}
	}
	if(para=='hint'){
		document.getElementById("btn-hint").style.display = "inline";
	}
}

function setsubEditor(para)
{
	var value = $('#'+para).val();
	$('.Editor-editor').attr('id','Editor-editor');
         CKEDITOR.instances['ques_name'].setData(value);
    
	document.getElementById("btn-question").style.display = "none";
	document.getElementById("btn-answer").style.display = "none";
	document.getElementById("btn-hint").style.display = "none";
	
	if(para=='question'){
		document.getElementById("btn-question").style.display = "inline";
	}else if(para=='answer'){
		document.getElementById("btn-answer").style.display = "inline";
	}else if(para=='hint'){
		document.getElementById("btn-hint").style.display = "inline";
	}
}

function setInputArea(para)
{
	var value = $("#"+para).val();
	CKEDITOR.instances['ques_name'].setData(value);
	
	document.getElementById("btn-question").style.display = "none";
	document.getElementById("btn-option1").style.display = "none";
	document.getElementById("btn-option2").style.display = "none";
	document.getElementById("btn-option3").style.display = "none"; 
	document.getElementById("btn-option4").style.display = "none";
	document.getElementById("btn-hint").style.display = "none";
	document.getElementById("btn-ma").style.display = "none";
	document.getElementById("btn-mb").style.display = "none";
	document.getElementById("btn-mc").style.display = "none";
	document.getElementById("btn-md").style.display = "none";
	document.getElementById("btn-me").style.display = "none";
	document.getElementById("btn-mp").style.display = "none";
	document.getElementById("btn-mq").style.display = "none";
	document.getElementById("btn-mr").style.display = "none";
	document.getElementById("btn-ms").style.display = "none";
	document.getElementById("btn-mt").style.display = "none";
	if(para=='ma'){
		document.getElementById("btn-ma").style.display = "inline";
	}else if(para=='mb'){
		document.getElementById("btn-mb").style.display = "inline";
	}else if(para=='mc'){
		document.getElementById("btn-mc").style.display = "inline";
	}else if(para=='md'){
		document.getElementById("btn-md").style.display = "inline";
	}else if(para=='me'){
		document.getElementById("btn-me").style.display = "inline";
	}else if(para=='mp'){
		document.getElementById("btn-mp").style.display = "inline";
	}else if(para=='mq'){
		document.getElementById("btn-mq").style.display = "inline";
	}else if(para=='mr'){
		document.getElementById("btn-mr").style.display = "inline";
	}else if(para=='ms'){
		document.getElementById("btn-ms").style.display = "inline";
	}else if(para=='mt'){
		document.getElementById("btn-mt").style.display = "inline";
	}
}
/**
 * setTextArea is an javascript funtion to collect data from text area to there respective field.
 * @param {string} para [id of textarea]
 */
function setTextArea(para)
{
	var param = '#'+para;
	var value =CKEDITOR.instances.ques_name.getData();// $('#Editor-editor').html();
	// $('#Editor-editor').html();
	if(para=='question'){
		document.getElementById("question").value = value;
	}else if(para=='option1'){
		document.getElementById("option1").value = value;
	}else if(para=='option2'){
		document.getElementById("option2").value = value;
	}else if(para=='option3'){
		document.getElementById("option3").value = value;
	}else if(para=='option4'){
		document.getElementById("option4").value = value;
	}else if(para=='hint'){
		document.getElementById("hint").value = value;
	}else if(para=='ma'){
		document.getElementById("ma").value = value;
		document.getElementById("ma").removeAttribute("readonly");
	}else if(para=='mb'){
		document.getElementById("mb").value = value;
		document.getElementById("mb").removeAttribute("readonly");
	}else if(para=='mc'){
		document.getElementById("mc").value = value;
		document.getElementById("mc").removeAttribute("readonly");
	}else if(para=='md'){
		document.getElementById("md").value = value;
		document.getElementById("md").removeAttribute("readonly");
	}else if(para=='me'){
		document.getElementById("me").value = value;
		document.getElementById("me").removeAttribute("readonly");
	}else if(para=='mp'){
		document.getElementById("mp").value = value;
		document.getElementById("mp").removeAttribute("readonly");
	}else if(para=='mq'){
		document.getElementById("mq").value = value;
		document.getElementById("mq").removeAttribute("readonly");
	}else if(para=='mr'){
		document.getElementById("mr").value = value;
		document.getElementById("mr").removeAttribute("readonly");
	}else if(para=='ms'){
		document.getElementById("ms").value = value;
		document.getElementById("ms").removeAttribute("readonly");
	}else if(para=='mt'){
		document.getElementById("mt").value = value;
		document.getElementById("mt").removeAttribute("readonly");
	}
}

function setparaTextArea(para)
{
	
	var value =CKEDITOR.instances.ques_name.getData();
	for(var i=1;i<=4;i++){
		if(para=='question'+i){
			document.getElementById("question"+i).value = value;
		}
	}
	for(var j=1;j<=16;j++){
		if(para=='option'+j){
			document.getElementById("option"+j).value = value;
		}
	}
	if(para=='hint'){
		document.getElementById("hint").value = value;
	}
}

function setsubTextArea(para)
{	
	var value =CKEDITOR.instances.ques_name.getData();
	if(para=='question'){
		document.getElementById("question").value = value;
	}else if(para=='answer'){
		document.getElementById("answer").value = value;
	}else if(para=='hint'){
		document.getElementById("hint").value = value;
	}
}

function getChecked(para,id,ans)
{
	var param = '#'+para;
	var checked = document.getElementById(para).checked;
	//alert(checked);
	var value = document.getElementById(para).value;
	if(checked == true){
		document.getElementById(id).value += value;
		document.getElementById(ans).value += value;
	}else{
		var str = document.getElementById(id).value;
		var value = str.replace(value, '');
		document.getElementById(id).value = value;
		document.getElementById(ans).value = value;
	}
}
