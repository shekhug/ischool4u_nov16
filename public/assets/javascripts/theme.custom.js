/* Add here all your JS customizations */

var URL = 'http://'+$(location).attr('hostname')+"/ischool4u";
function onlyalpha(text){
    var texts = $('#'+text).val();
    texts = texts.replace(/\s/g, "");
    var rag = /(\W|_|\d)/g;
    var result = rag.test(texts);
    if(result==true){
        $("#"+text).addClass("border-red");
        $("#alpha").html("<p class='text-danger' style='padding:0;margin:0;'>Only Alphabet</p>");
    }else{
        $("#"+text).removeClass("border-red");
        $("#alpha").html("");
    }
}

function clickoption(qid,ans,count){
    var select = $("#"+qid).val();
    var patt = new RegExp(ans);
    var res = patt.exec(select);
    if(res==null){
        $("#"+qid+ans).addClass("option-selected");
        select = select+ans;
        $("#"+qid).val(select);
    }else{
        $("#"+qid+ans).removeClass("option-selected");
        select = select.replace(ans,"");
        $("#"+qid).val(select);
    }
    $("#secol"+count+" .badge").css("background-color","#97E4A0");
}

function oneclickoption(qid,ans,count){
    var array = ["A","B","C","D"];
    for (i = 0; i <= 3; i++) {
        $("#"+qid+array[i]).removeClass("option-selected");
    }
    $("#"+qid+ans).addClass("option-selected");
    $("#"+qid).val(ans);
    $("#secol"+count+" .badge").css("background-color","#97E4A0");
}

function checkoneans(id,notication){
    var ans = $("#"+id+"ans").val();
    var checkans = $("#"+id).val();
    var checkid = 1;
    if(ans == checkans){
        $("#"+id+checkans).removeClass("option-selected");
        $("#"+id+checkans).addClass("right-ans");
        $("#"+id+checkans).html($("#"+id+checkans).html()+"<p class='option-label-right pull-right appear-animation fadeInLeft'><i class='fa fa-check'></i> Your Answer Right</p><div class='clearfix'></div>");
        $("#"+id+'submit').addClass("hidden");
        var array = ["A","B","C","D"];
        for (i = 0; i <= 3; i++) {
            $("#"+id+array[i]).removeAttr("onclick");
        }
    }else{
        $("#"+id+checkans).removeClass("option-selected");
        $("#"+id+checkans).addClass("wrong-ans");
        $("#"+id+ans).addClass("right-ans");
        $("#"+id+checkans).html($("#"+id+checkans).html()+"<p class='option-label-wrong pull-right appear-animation fadeInLeft'> <i class='fa fa-times'></i> Your Answer Wrong</p><div class='clearfix'></div>");
        $("#"+id+ans).html($("#"+id+ans).html()+"<p class='option-label-right pull-right appear-animation fadeInLeft'><i class='fa fa-check'></i> Right Answer</p><div class='clearfix'></div>");
        $("#"+id+'submit').addClass("hidden");
        var array = ["A","B","C","D"];
        for (i = 0; i <= 3; i++) {
            $("#"+id+array[i]).removeAttr("onclick");
            $("#"+id+array[i]).removeClass("options option-selected");
        }
        checkid = 2;
    }
    if(checkid==2){
        $("#"+id+'aoua').removeClass('unattempt right');
        $("#"+id+'aoua').addClass("wrong");
    }else{
        $("#"+id+'aoua').removeClass('unattempt wrong');
        $("#"+id+'aoua').addClass("right");
    }
    //hvjh13300028302
    // ajax call start here
    // 
    $.ajax({
        type: 'POST',
        url: URL+'/bookmarks/attempt',
        data: 'qid='+id+'&checkid='+checkid+'&giveans='+checkans+'&notication='+notication,
        success: function(data){
            if(data){
                console.log(data);
            }
        }
    });
}

function checkmultians(id,notication){
    var rans = $("#"+id+"ans").val();
    var sans = $("#"+id).val().split("");
    var checkid = 1;
    if(sans==null){
    }else{
        for (i=0;i<=sans.length-1;i++){
            var patt = new RegExp(sans[i]);
            var res = patt.test(rans);
            if(res==true){
                $("#"+id+sans[i]).addClass("right-ans");
                $("#"+id+sans[i]).html($("#"+id+sans[i]).html()+"<p class='option-label-right pull-right appear-animation fadeInLeft'><i class='fa fa-check'></i> Your Answer Right</p><div class='clearfix'></div>");
            }else if(res==false){
                $("#"+id+sans[i]).addClass("wrong-ans");
                $("#"+id+sans[i]).html($("#"+id+sans[i]).html()+"<p class='option-label-wrong pull-right appear-animation fadeInLeft'> <i class='fa fa-times'></i> Your Answer Wrong</p><div class='clearfix'></div>");
                checkid = 2;
            }
        }
    }
    ranss = rans.split("");
    for(i = 0; i <= ranss.length-1; i++){
        if($("#"+id+ranss[i]).hasClass("right-ans")!=true){
            $("#"+id+ranss[i]).addClass("right-ans");
            $("#"+id+ranss[i]).html($("#"+id+ranss[i]).html()+"<p class='option-label-right pull-right appear-animation fadeInLeft'><i class='fa fa-check'></i> Right Answer</p><div class='clearfix'></div>");
        };
    }
    var array = "ABCD".split("");
    for (i = 0; i <= 3; i++) {
        $("#"+id+array[i]).removeAttr("onclick");
        $("#"+id+array[i]).removeClass("options option-selected");
    }
    $("#"+id+'submit').addClass("hidden");
    if(checkid==2){
        $("#"+id+'aoua').removeClass('unattempt right');
        $("#"+id+'aoua').addClass("wrong");
    }else{
        $("#"+id+'aoua').removeClass('unattempt wrong');
        $("#"+id+'aoua').addClass("right");
    }
    //
    // ajax call start here
    // 
    $.ajax({
        type: 'POST',
        url: URL+'/bookmarks/attempt',
        data: 'qid='+id+'&checkid='+checkid+'&giveans='+sans+'&notication='+notication,
        success: function(data){
            if(data){
                console.log(data);
            }
        }
    });
}

function checknuans(id,notication){
    var ans = $("#"+id+"ans").val();
    var checkans = $("#"+id).val();
    if(ans == checkans){
        // $(".reason"+id).html($(".reason"+id).html()+"<p class='option-label-right'><i class='fa fa-check'></i> Your Answer Right</p><div class='clearfix'></div>");
        $("#"+id+'submit').addClass("hidden");
        $("#alert"+id).addClass("has-success");
        $("#"+id).attr("id","inputSuccess "+id);
    }else{
        $(".reason"+id).html($(".reason"+id).html()+"<input class='form-control' id='inputSuccess' value='"+ans+"'>");
        $("#"+id+'submit').addClass("hidden");
        $("#alert"+id).addClass("has-error");
        $("#"+id).attr("id","inputError "+id);
        $(".reason"+id).addClass("has-success");
    }
    if(notification!=''){
        $.ajax({
            type: 'POST',
            url: URL+'/bookmarks/notification',
            data: 'qid='+id+'&giveans='+checkans+'&notication='+notication,
            success: function(data){
                if(data){
                    console.log(data);
                }
            }
        });
    }
}

function checkmatchans(id){
    var pans = "pqrst".split(""); // Posible Answer
    var abc = 'abcde'.split(""); // use for
    var rightans,splitans,uans;
    if($("#"+id+"ans5").val()==''){
        for(i=0;i<=3;i++){
            for (j=0; j<=3; j++) {
                rightans = $("#"+id+'ans'+(i+1)).val();
                splitans = rightans.split("");
                for(k=1;k<=splitans.length;k++){
                    if($("#"+id+abc[i]+pans[j]).is(":checked")){
                        uans = $("#"+id+abc[i]+pans[j]).val();
                        if(uans==splitans[k-1]){
                            $("#"+id+'alert'+abc[i]+pans[j]).removeClass("checkbox-default");
                            $("#"+id+'alert'+abc[i]+pans[j]).addClass("checkbox-success");
                        }else{
                            $("#"+id+'alert'+abc[i]+pans[j]).removeClass("checkbox-default");
                            $("#"+id+'alert'+abc[i]+pans[j]).addClass("checkbox-danger");
                        }
                    }
                }
            }
            $("#"+id+'right'+abc[i]).html('Correct Ans: '+rightans);
        }
    }
    $("#"+id+'submit').addClass("hidden");
	
	
}

function snote(id){
    var snote = $(".snote"+id).val();
    var note = $("#note"+id).val();
    var ntype = $("#ntype"+id).val();
    if(note!=''){
    $("#note"+id).val("");
    $(".snote"+id).html('<div class="clearfix">'+$(".snote"+id).html()+note+'</div>');
    $.ajax({
        type: 'POST',
        url: URL+'/bookmarks/snote',
        data: 'qid='+id+'&note='+note+'&type='+ntype,
        success: function(data){
            if(data){
                $("#solu"+id).modal("hide");
                console.log(data);
            }
        }
    });
}else{
    alert("Please Enter Note");
    return false;
}
}

function dout(id){
    var dout = $(".doutmcon"+id).val();
    var note = $("#douta"+id).val();
     if(note!=''){
    $("#douta"+id).val("");
    $(".doutmcon"+id).html('<div class="clearfix">'+$(".doutmcon"+id).html()+'<p><i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>'+note+'</p></div>');
    $.ajax({
        type: 'POST',
        url: URL+'/bookmarks/dnote',
        data: 'qid='+id+'&note='+note,
        success: function(data){
            if(data){
                $(".dobshw"+id).html("Doubt Added successfully.");
                $("#doubt"+id).modal("hide");
                console.log(data);
            }
        }
    });
}else{
    $(".dobshw"+id).html("Please enter the Doubt in Question.");
    return false;
}
}
function eout(id){
    var eout = $(".er"+id).val();
    var note = $("#err"+id).val();
    if(note!=''){
    $("#err"+id).val("");
    $(".er"+id).html('<div class="clearfix">'+$(".er"+id).html()+'<p><i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>'+note+'</p></div>');
    $.ajax({
        type: 'POST',
        url: URL+'/bookmarks/enote',
        data: 'qid='+id+'&note='+note,
        success: function(data){
            if(data){
                $(".errshw"+id).html("Error Added successfully.");
                $("#error"+id).modal("hide");
                console.log(data);
            }
        }
    });
}else{
    $(".errshw"+id).html("Please enter the Error in Question.");
    return false;
}
}

function question_select(){
    $(".question_select").toggle();
	
}

$(document).ready(function(){
    getnotification();
    getinvitefriend();
    setInterval(function(){
        getnotification();
        getinvitefriend();
    },5000);
});

function  getnotification(){
    $.ajax({
        type: 'POST',
        url: URL+'/socialisation/getnotification',
        //data: 'qid=1',
        success: function(data){
            if(data){
                $("#notification").html(data);
            }
        }
    });
}
function  getinvitefriend(){
    $.ajax({
        type: 'POST',
        url: URL+'/socialisation/getinvitefriend',
        //data: 'qid=1',
        success: function(data){
            if(data){
                $("#invitefriend").html(data);
            }
        }
    });
}

function fixform(){
    document.getElementById("fixpackages").submit();
}

// by satya....
function changeDate(quest){
    $("#sho"+quest).attr("style","display:none");
    $("#chn"+quest).removeAttr("style");
}
function savedate(quest){
    var date=$("#val"+quest).val();
    if(date!=''){
        $.ajax({
            type: 'POST',
            url: URL+'/sitesetting/savedate',
            data: 'qid='+quest+"&date="+date,
            success: function(data){
                if(data!='1'){
                    $("#val"+quest).val(data);
                    $("#up"+quest).html(data);
                    $("#sho"+quest).removeAttr("style");
                    $("#btn"+quest).html("Change");
                    $("#newcl"+quest).html('');
                    $("#newcl"+quest).html('<a class="btn btn-info btn-xs" onclick=cleardate("'+quest+'")>Clear</a>');
                    $("#chn"+quest).attr("style","display:none");
                }else{
                    $("#val"+quest).val(date);
                    $("#val"+quest).addClass("btn-danger");
                    $("#val"+quest).focus();
                    alert("Already Assigned the date");
                }
            }
        });
    }else{
        $("#val"+quest).addClass("btn-warning");
        $("#val"+quest).focus();
    }
}
function cleardate(quesid){
    $.ajax({
        type: 'POST',
        url: URL+'/sitesetting/cleardate',
        data: 'qid='+quesid,
        success: function(data){
            $("#chn"+quesid).css("display","none");
            $("#clr"+quesid).html('');
            $("#val"+quesid).val('');
            $("#up"+quesid).html('');
            $("#newcl"+quesid).html('');
            $("#clr"+quesid).html('<button onclick=changeDate("'+quesid+'"); class="btn btn-info btn-xs" id="btn'+quesid+'">Assign</button>');
        }
    })
}
function removequiz(quesid){
    $.ajax({
        type: 'POST',
        url: URL+'/sitesetting/removequiz',
        data: 'qid='+quesid,
        success: function(data){
            $("#upaa"+quesid).attr("title","Click to Add");
            $("#upaa"+quesid).attr("onclick","addquiz('"+quesid+"');");
            $("#upaa"+quesid).html("Add");
        }
    });
}
function addquiz(quesid){
    $.ajax({
        type: 'POST',
        url: URL+'/sitesetting/addquiz',
        data: 'questionid='+quesid,
        success: function(data){
            $("#upaa"+quesid).attr("title","Click to Remove");
            $("#upaa"+quesid).attr("onclick","removequiz('"+quesid+"');");
            $("#upaa"+quesid).html("Assigned");
        }
    });
}


function followfriend(userid){
    $.ajax({
        type: 'POST',
        url: URL+'/index/followus',
        data: 'uid='+userid,
        success: function(data){
            window.location.href='';
        //$("#fld"+userid).html("Following");
        }
    });
}
function likedata(id_blog){
    $.ajax({
        type: 'POST',
        url: URL+'/index/likeblog/',
        data: 'id_blog='+id_blog,
        success: function(data){ 
            window.location.href='';
        }
    });
}
function exam_show() {
       
    var y = screen.width;
    if (y < 992) {
            
        document.getElementById('exam').style.display = "block"; 
        document.getElementById("exam").className = "col-md-3 qs-bar question-section animated slideInLeft";
        
    }
}
function hide_exam() {
    document.getElementById("exam").className = "col-md-3 qs-bar question-section animated slideOutLeft";
    document.getElementById('exam').style.display = "none"; 
}
function getnewsubsub(subid){
    $.ajax({
        type: 'POST',
        url: URL+'/questionbank/getsubsubject',
        data: 'subjectid='+subid,
        success: function(data){
            if(data){
                $("#subsubject").html(data);
                $("#topics").html("<option>--Select Topics--</option>");
                $("#subtopic").html("<option>--Select SubTopics--</option>");
            }
        }
    });
}
function getnewtopics(ssubid){
    $.ajax({
        type: 'POST',
        url: URL+'/questionbank/gettopics',
        data: {
            ssubid:ssubid
        },
        success: function(data){
            if(data){
                $("#topics").html(data);
                $("#subtopic").html("<option>--Select SubTopics--</option>");
            }
        }
    });
}
function getnewsubtopic(topics){
    $.ajax({
        type: 'POST',
        url: URL+'/questionbank/getsubtopics',
        data: {
            topics:topics
        },
        success: function(data){
            if(data){
                $("#subtopic").html(data);
            }
        }
    });
}
function createPracList(){
    var formRecord = $("#pracAddForm").serialize();
    $(".load_box").css("display","flex");
    $.ajax({
        type: 'POST',
        url: URL+'/exams/addpractice',
        data: {
            formRecord:formRecord
        },
        success: function(data){
            if(data){
                 $(".load_box").css("display","none");
                    if(data==1){
                window.location.href="/ischool4u/exams/practice";
            }else if(data==2){
                alert("Error in creating practice Test! Please Try Again");
            }else{
                window.location.href="/ischool4u/exams/practice/"+data;
            }
            }
        }
    });
}
function updatePracList(){
    var formRecord = $("#pracupForm").serialize();
    $(".load_box").css("display","flex");
    $.ajax({
        type: 'POST',
        url: URL+'/exams/updatepractice',
        data: {
            formRecord:formRecord
        },
        success: function(data){
            if(data){
                $(".load_box").css("display","none");
                if(data==1){
                window.location.href="/ischool4u/exams/practice";
            }else{
                window.location.href="/ischool4u/exams/practice/"+data;
            }
            }
        }
    });
}   
function getdoubQueDetails(questionID,qsID){//alert(questionID);
    $.ajax({
        type: 'POST',
        url: URL+'/questions/getQueDetails',
        data: {questionID:questionID},
        success: function(data){
            if(data){
                $("#question_Details_"+qsID).html(data);
            }
        }
    });
}  
function getErrQueDetails(questionID,qsID) {
    $.ajax({
        type: 'POST',
        url: URL+'/questions/getQueDetails',
        data: {questionID:questionID},
        success: function(data){
            if(data){
                $("#question_ErrDetails_"+qsID).html(data);
            }
        }
    });
}        
function checklogin(){
    var logval=validatelogin();
    var email=$("#email").val();
    var passoword=$("#pass").val();
    if(logval){
    $.post(URL+"/index/checklogin/",{"pass":passoword,"email":email},function(res){
            if(res==1){ 
            document.getElementById("newloginform").submit();
            }else{
                $(".creden-error").css("display","block");
            }
        });
    }
}