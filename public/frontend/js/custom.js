// Add here all your JS customizations
function disnone(){
	$("#login").fadeOut('1000');
	$(".course-form").fadeOut();
	$(".course-list").fadeOut();
	$(".signuptitle").fadeOut();
	$(".logintitle").fadeOut();
	$(".login-form").fadeOut();
	$(".message").fadeOut();
	$(".course-list2").fadeOut();
	$("#body-cover").delay('300').fadeOut('1000');
}
function onshows(){
	$("#login").fadeIn('1000');
	$("#body-cover").fadeIn('1000');
	$(".signuptitle").fadeIn();
	$(".course-list").fadeIn();
}
function checklogin(){

	$("#login").fadeIn('1000');
	$("#body-cover").fadeIn('1000');
	$(".logintitle").fadeIn();
	$(".login-form").fadeIn();
}
function login(){
	$(".signuptitle").fadeOut();
	$(".course-form").fadeOut();
	$(".logintitle").fadeIn('1000');
	$(".login-form").fadeIn('1000');
}
 
